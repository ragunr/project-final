import flash.media.Sound;
import flash.media.SoundChannel;
import starling.display.*;
import starling.animation.*;
import starling.events.*;
import starling.textures.*;
import flash.geom.*;
import box2D.dynamics.*;
import box2D.common.math.*;
import box2D.collision.shapes.*;
import Game;
import Geom;
import Tilemap;
import Light;
using Geom.Vec2Ext;
using Geom.RectExt;
using Lambda;

class Player extends PhysicsEntity {
	private var speed : Float = 150.0;	//pixels moved per second
    public var faceForward : Bool = true;
    private var flashlight : LightBeam; 
    private var negativeForce: Int;
    public var activateEntity: Entity;

    private var ladderVel: Float;
    public var flashlightEnabled : Bool = true;
    public var runEnabled : Bool = true;
    public var running : Bool = false;

    public var noFlash: Bool = false;

    private var sounds: SoundEffects;
	
	public var walking: SoundChannel;

	public function new(position:B2Vec2, game:Game){
		sprite_lit = new AnimSprite();
		sprite_dark = new AnimSprite();
		
		//saved so we can access the width and height
		var playerImageLit = new Image(Root.assets.getTexture("playerTestLit"));
		
		//sprite_lit.addChild(playerImageLit);
		//sprite_dark.addChild(new Image(Root.assets.getTexture("playerTestDark")));
		
		var bodyDef = new B2BodyDef();
		bodyDef.type = DYNAMIC_BODY;
		bodyDef.fixedRotation = true;

        //setupAnimations();
		
		super(position, sprite_lit, sprite_dark, bodyDef, game);
        setCategory(CollisionFilters.Player);
		setMask(CollisionFilters.Platform | CollisionFilters.Foreground);
		
		//Everything from here down is just temporary values for testing purposes. THESE ARE NOT CORRECT VALUES!
		var playerCircle = new B2CircleShape();
		playerCircle.setRadius(playerImageLit.width / 2 / Game.PPM);
		playerCircle.setLocalPosition(new B2Vec2(0, (playerImageLit.height / 2 - playerImageLit.width / 2) / Game.PPM));
		
		var circleFixtureDef = new B2FixtureDef();
		circleFixtureDef.shape = playerCircle;
		circleFixtureDef.friction = 0.0;
		circleFixtureDef.restitution = 0.0;
		createFixture(circleFixtureDef);
		
		var playerBox = new B2PolygonShape();
		playerBox.setAsOrientedBox(playerImageLit.width / 2 / Game.PPM, (playerImageLit.height - playerImageLit.width / 2) / 2 / Game.PPM, new B2Vec2(0, -(playerImageLit.height / 2 - (playerImageLit.height - playerImageLit.width / 2) / 2) / Game.PPM));
		playerBox.setAsOrientedBox((playerImageLit.width / 2 - 2) / Game.PPM, (playerImageLit.height - playerImageLit.width / 2) / 2 / Game.PPM, new B2Vec2(0, -playerImageLit.width / 4 / Game.PPM));
		
		var boxFixtureDef = new B2FixtureDef();
		boxFixtureDef.shape = playerBox;
		boxFixtureDef.density = 64.0;	// Average adult weighs 64kgs. We can toy with this as needed
		boxFixtureDef.friction = 0.0;	// Player should not be affected by friction
		boxFixtureDef.restitution = 0.0;
		createFixture(boxFixtureDef);

        //sounds = new SoundEffects();



        setupAnimations();

        flashlight = new LightBeam(game, {x:1,y:0});

        ladderVel = 0;
    }

	private function setupAnimations(){
		var anim;
		
		anim = new MovieClip(Root.assets.getTextures("ProIdleB"),4);
		anim.alignPivot();
		sprite_lit.addChild(anim);
		sprite_lit.addAnimation("idle",anim);
		anim = new MovieClip(Root.assets.getTextures("ProIdleB"),4);
		anim.alignPivot();
        anim.color = 0xAAAAAA;
		sprite_dark.addChild(anim);
		sprite_dark.addAnimation("idle",anim);

		anim = new MovieClip(Root.assets.getTextures("Pro_walk"));
		anim.alignPivot();
		sprite_lit.addChild(anim);
		sprite_lit.addAnimation("walk",anim);
		anim = new MovieClip(Root.assets.getTextures("Pro_walk"));
		anim.alignPivot();
        anim.color = 0xAAAAAA;
		sprite_dark.addChild(anim);
		sprite_dark.addAnimation("walk",anim);

		anim = new MovieClip(Root.assets.getTextures("ProClimb"));
		anim.alignPivot();
                anim.y = -36;
		sprite_lit.addChild(anim);
		sprite_lit.addAnimation("climb",anim);
		anim = new MovieClip(Root.assets.getTextures("ProClimb"));
		anim.alignPivot();
                anim.y = -36;
        anim.color = 0xAAAAAA;
		sprite_dark.addChild(anim);
		sprite_dark.addAnimation("climb",anim);

		anim = new MovieClip(Root.assets.getTextures("LedgeClimb"),14);
		anim.alignPivot();
                anim.y = -72;
		sprite_lit.addChild(anim);
		sprite_lit.addAnimation("pullup",anim);
		anim = new MovieClip(Root.assets.getTextures("LedgeClimb"),14);
		anim.alignPivot();
                anim.y = -72;
        anim.color = 0xAAAAAA;
		sprite_dark.addChild(anim);
		sprite_dark.addAnimation("pullup",anim);

		anim = new MovieClip(Root.assets.getTextures("TeenPul"),6);
		anim.alignPivot();
        anim.y = 0;
		sprite_lit.addChild(anim);
		sprite_lit.addAnimation("save_teen",anim);
		anim = new MovieClip(Root.assets.getTextures("TeenPul"),6);
		anim.alignPivot();
        anim.y = 0;
		sprite_dark.addChild(anim);
        anim.color = 0xAAAAAA;
		sprite_dark.addAnimation("save_teen",anim);

		sprite_lit.playAnimation("idle",{x:1,y:1},true);
		sprite_dark.playAnimation("idle",{x:1,y:1},true);
		
	}
	
	public function stopMoving() {
            setLinearVelocity(new B2Vec2(0,0));
            ladderVel = 0;
            var facing_scale:Vec2 = {x:(faceForward)? 1 : -1, y:1};
            if(game.ladder > 0){
                sprite_lit.pauseAnimation();
                sprite_dark.pauseAnimation();
            } else {
                playAnimation("idle",facing_scale);
            }

            //game.sounds.stopWalk();
			if (walking != null) {
				walking.stop();
				walking = null;
			}


	}


	public function aimFlashlight(direction:Direction)
	{
		var facing_coef = (faceForward) ? 1 : -1;
		if(direction == Down){
			flashlight.direction = {x:facing_coef,y:1.00}
		} 
		else if(direction == Up){
			flashlight.direction = {x:facing_coef,y:-3}
		} 
		else {
			flashlight.direction = {x:facing_coef,y:0.1}
		}
	}
	
	public function move(direction:Direction) {
        var running_coef = (running) ? 2 : 1;
            if (direction == Left) { 
                faceForward = false;
                flashlight.direction = {x:-1,y:0};
                setLinearVelocity(new B2Vec2(-1 * speed*running_coef, 0));
            }
            if (direction == Right) { 
                faceForward = true;
                flashlight.direction = {x:1,y:0};
                setLinearVelocity(new B2Vec2(1 * speed*running_coef, 0));
            }
            if(direction == Up){
            	faceForward = true;
                flashlight.direction = {x:1,y:0};
                ladderVel = (-0.7 * speed) / Game.PPM;
            }
            if(direction == Down){
            	faceForward = true;
                flashlight.direction = {x:1,y:0};
                ladderVel = (0.5 * speed) / Game.PPM;            }
            
            var facing_scale:Vec2 = {x:(faceForward)? 1 : -1, y:1};
            if(game.ladder > 1 && game.onFloor == false) {
                sprite_lit.playAnimation("climb",facing_scale);
                sprite_dark.playAnimation("climb",facing_scale);
                if(direction == Left || direction == Right){
                    sprite_lit.pauseAnimation();
                    sprite_dark.pauseAnimation();
                }
            } else {
                sprite_lit.playAnimation("walk",facing_scale);
                sprite_dark.playAnimation("walk",facing_scale);
            }
			if (walking == null) {
				var w = Root.assets.getSound("walking1");
				walking = w.play();	
			}
		   

	}

    public function activate(button_value : Float){
        if(activateEntity == null) return;
        if(button_value == 1) 
            if(activateEntity.beginActivateCallback != null) activateEntity.beginActivateCallback();
        else
            if(activateEntity.endActivateCallback != null) activateEntity.endActivateCallback();
    }

    public function toggleFlashlight(){
        if(flashlight.light_on) flashlight.switch_off();
        else if(flashlightEnabled) flashlight.switch_on();
    }

    public function getCameraFocus():Vec2 {
        var facing_coef = (faceForward) ? 1 : -1;
        return {x:position.x+50*facing_coef, y:position.y};
    }

    override function advanceTime(t:Float){
        flashlight.advanceTime(t);
        if(running) t*=2;
        super.advanceTime(t);
        flashlight.position = {x:position.x+(faceForward ? 18 : -18),y:position.y-16};

        if(game.ladder > 0){

            var curVel = body.getLinearVelocity();
            curVel.y = ladderVel - .25;

            if(ladderVel == 0 ){
                game.onFloor = true;
            }
            else{
                game.onFloor = false;
            }
            
            body.setLinearVelocity(curVel);
        }

        if(noFlash){
            flashlight = new LightBeam(game, {x:0,y:0});
        }

        
    }
}
