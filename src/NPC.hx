import starling.display.*;
import starling.animation.*;
import starling.events.*;
import starling.textures.*;
import flash.geom.*;
import box2D.dynamics.*;
import box2D.common.math.*;
import box2D.collision.shapes.*;
import Game;
import Geom;
import Tilemap;
import Light;
using Geom.Vec2Ext;
using Geom.RectExt;
using Lambda;

class NPC extends PhysicsEntity {
	public var speed : Float = 100.0;	//pixels moved per second
    public var faceForward : Bool = true;

    private var sounds: SoundEffects;

	public function new(position:B2Vec2, animations:Dynamic, dark_shade:Int, game:Game){
		sprite_lit = new AnimSprite();
		sprite_dark = new AnimSprite();

        setupAnimations(animations,dark_shade);
		
		//saved so we can access the width and height
		var NPCImageLit = new Image(Root.assets.getTexture(animations.stand));
		
		var bodyDef = new B2BodyDef();
		bodyDef.type = DYNAMIC_BODY;
		bodyDef.fixedRotation = true;
		
		super(position, sprite_lit, sprite_dark, bodyDef, game);
        setCategory(CollisionFilters.NPC);
		setMask(CollisionFilters.Platform);
		
		//Everything from here down is just temporary values for testing purposes. THESE ARE NOT CORRECT VALUES!
		var NPCCircle = new B2CircleShape();
		NPCCircle.setRadius(NPCImageLit.width / 2 / Game.PPM);
		NPCCircle.setLocalPosition(new B2Vec2(0, (NPCImageLit.height / 2 - NPCImageLit.width / 2) / Game.PPM));
		
		var circleFixtureDef = new B2FixtureDef();
		circleFixtureDef.shape = NPCCircle;
		circleFixtureDef.friction = 0.0;
		circleFixtureDef.restitution = 0.0;
		createFixture(circleFixtureDef);
		
		var NPCBox = new B2PolygonShape();
		NPCBox.setAsOrientedBox((NPCImageLit.width / 2 - 2) / Game.PPM, (NPCImageLit.height - NPCImageLit.width / 2) / 2 / Game.PPM, new B2Vec2(0, -NPCImageLit.width / 4 / Game.PPM));
		
		var boxFixtureDef = new B2FixtureDef();
		boxFixtureDef.shape = NPCBox;
		boxFixtureDef.density = 64.0;	// Average adult weighs 64kgs. We can toy with this as needed
		boxFixtureDef.friction = 0.0;	// NPC should not be affected by friction
		boxFixtureDef.restitution = 0.0;
		createFixture(boxFixtureDef);

        sounds = new SoundEffects();
    }

	private function setupAnimations(animations:Dynamic, dark_shade:Int){
		var anim;
		//trace(animations.talk);
		anim = new MovieClip(Root.assets.getTextures(animations.talk));
		anim.alignPivot();
		sprite_lit.addChild(anim);
		sprite_lit.addAnimation("talk", anim);
		anim = new MovieClip(Root.assets.getTextures(animations.talk));
		anim.alignPivot();
        anim.color = dark_shade;
		sprite_dark.addChild(anim);
		sprite_dark.addAnimation("talk", anim);
		
        //trace(animations.walk);
		anim = new MovieClip(Root.assets.getTextures(animations.walk));
		anim.alignPivot();
        anim.y = 9;
		sprite_lit.addChild(anim);
		sprite_lit.addAnimation("walk",anim);
		anim = new MovieClip(Root.assets.getTextures(animations.walk));
		anim.alignPivot();
        anim.y = 9;
        anim.color = dark_shade;
		sprite_dark.addChild(anim);
		sprite_dark.addAnimation("walk",anim);

        //trace(animations.stand);
		anim = new MovieClip(Root.assets.getTextures(animations.stand));
		anim.alignPivot();
		sprite_lit.addChild(anim);
		sprite_lit.addAnimation("stand",anim);
		anim = new MovieClip(Root.assets.getTextures(animations.stand));
		anim.alignPivot();
        anim.color = dark_shade;
		sprite_dark.addChild(anim);
		sprite_dark.addAnimation("stand",anim);

		sprite_lit.playAnimation("stand",{x:1,y:1},true);
		sprite_dark.playAnimation("stand",{x:1,y:1},true);
		
	}
	
	public function talk() {
		var facing_scale:Vec2 = {x:(faceForward)? 1 : -1, y:1};
		sprite_lit.playAnimation("talk", facing_scale);
		sprite_dark.playAnimation("talk", facing_scale);
	}
	public function stopTalking() {
		var facing_scale:Vec2 = {x:(faceForward)? 1 : -1, y:1};
		sprite_lit.playAnimation("stand", facing_scale);
		sprite_dark.playAnimation("stand", facing_scale);
	}
	
	public function stopMoving() {
		setLinearVelocity(new B2Vec2(0,0));
		var facing_scale:Vec2 = {x:(faceForward)? 1 : -1, y:1};
		sprite_lit.playAnimation("stand",facing_scale);
		sprite_dark.playAnimation("stand",facing_scale);

	}
	
	public function move(direction:Direction) {
		if (direction == Left) { 
			faceForward = false;
			setLinearVelocity(new B2Vec2(-1 * speed, 0));
		}
		if (direction == Right) { 
			faceForward = true;
			setLinearVelocity(new B2Vec2(1 * speed, 0));
		}
		
		var facing_scale:Vec2 = {x:(faceForward)? 1 : -1, y:1};
		sprite_lit.playAnimation("walk",facing_scale);
		sprite_dark.playAnimation("walk",facing_scale);

	}

    override function advanceTime(t:Float){
        super.advanceTime(t);
    }
}
