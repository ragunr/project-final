import Game.Entity;
import box2D.dynamics.*;
import box2D.common.math.*;
import box2D.collision.shapes.*;
import starling.display.Quad;

class FixturePhysicsEntity extends Entity {
	private var fixture : B2Fixture;
	private var body : B2Body;
	
	private var colliderLines : Array<Quad>;
	private var debugColor = 0xffffff;
	
	public function new(fixtureDef:B2FixtureDef, body:B2Body, game:Game){
        super(game);
		colliderLines = new Array<Quad>();
		
		this.body = body;
        fixture = body.createFixture(fixtureDef);
        fixture.SetUserData(this);
    }

	public override function drawCollider() {
		if (fixture != null) {
			var shapeType = fixture.getType();
			if (shapeType == POLYGON_SHAPE) {
				var polygonShape = cast(fixture.getShape(), B2PolygonShape);
				var vertices = polygonShape.getVertices();
				if (vertices.length > 2) {
					for (i in 0...vertices.length - 1) {
						drawColliderSegment(vertices[i], vertices[i+1], i);
					}
					drawColliderSegment(vertices[vertices.length - 1], vertices[0], vertices.length - 1);
				}
			}
		}
	}
	
	private function drawColliderSegment(vert1:B2Vec2, vert2:B2Vec2, i:Int) {
		var pos1 = new B2Vec2((body.getPosition().x + vert1.x), (body.getPosition().y + vert1.y));
		var pos2 = new B2Vec2((body.getPosition().x + vert2.x), (body.getPosition().y + vert2.y));
		if (game.static_body.getAngle() != 0) {
			pos1 = Util.rotateAroundPoint(pos1, body.getPosition(), body.getAngle());
			pos2 = Util.rotateAroundPoint(pos2, body.getPosition(), body.getAngle());
		}
		pos1.x *= Game.PPM;
		pos1.y *= Game.PPM;
		pos2.x *= Game.PPM;
		pos2.y *= Game.PPM;
		var quad = Util.createLine(pos1, pos2, debugColor);
		colliderLines[i] = quad;
		game.debugLayer.addChild(quad);
	}
	
	public override function deleteCollider() {
		for (i in 0...colliderLines.length) {
			game.debugLayer.removeChild(colliderLines[i]);
		}
	}
}