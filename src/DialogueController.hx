import Game;
import Root;
import starling.utils.AssetManager;
import flash.utils.ByteArray;
import starling.core.Starling;
import Dialogue;
import haxe.xml.Fast;
import flash.utils.ByteArray;

using Lambda;

// a Dialogue has a title (speaker) and an array of lines.
// a Conversation has many dialogues inside from the same event.
typedef Conversation = { 
	var id: Int;  
	var autoPlay: Bool;
	var dialogues: Array<Dialogue>;
	@:optional var hasPlayed: Bool;
}

class DialogueController {

	public var conversations: Map<String, Conversation>;
	
	public function new(assets:AssetManager, xml: String) {
		// set up Conversation map
		conversations = new Map<String, Conversation>();
		
		// read in the XML file
		var xml = Xml.parse(haxe.Resource.getString(xml));
		var source = new Fast(xml.firstElement());

		// loop through Conversations
		for (c in source.nodes.Conversation) {
			// initialize this conversation
			var auto = false;
			if (c.has.autoPlay) {
				if (c.att.autoPlay == "true") auto = true;
			}
			var convo: Conversation = {id: Std.parseInt(c.att.id), dialogues: new Array<Dialogue>(), autoPlay: auto};
			var trig: String = c.node.Trigger.innerData;
			
			for (d in c.nodes.Dialogue) {
				// initialize this dialogue
				var dia = new Dialogue(d.node.Title.innerData, new List<String>());
				for (l in d.nodes.Line) {
					// save each line of the dialogue
					dia.text_queue.add(l.innerData);
				}
				convo.dialogues.push(dia);
			}
			conversations.set(trig, convo);
		}
		//trace("xml dialogue loaded.");
	}

	// called in Game to play dialogue.
	public function playDialogue(trigName:String) {
		//check hasPlayed to make sure dialogue isn't replayed
		var convo = conversations.get(trigName);
		
		return convo;
	}
	
}
