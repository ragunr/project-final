import haxe.xml.Fast;
import flash.utils.ByteArray;
import flash.geom.Rectangle;
import starling.display.Image;
import starling.display.Sprite;
import starling.textures.Texture;
import starling.textures.TextureSmoothing;
import starling.utils.AssetManager;

enum Orientation {
  Orthogonal;
  Isometric;
  IsometricStaggered;
  HexagonalStaggered;
}

enum RenderOrder {
  RightDown;
  RightUp;
  LeftDown;
  LeftUp;
}

private class Tile {
  public var id:Int;
  public var width:Int;
  public var height:Int;
  public var offset_x:Int = 0;
  public var offset_y:Int = 0;
  public var source:String;
  public var properties:Map<String,String>;
  // properties

  public function new() {
      properties = new Map<String,String>();
  }
}

private class Layer {
  public var name:String;
  public var data:Array<Array<Tile>>;
  public var visible:Bool;
  // properties

  public function new() {
    data = new Array<Array<Tile>>();
  }
}

private class Point {
    public var x:Float;
    public var y:Float;
    public function new(x:Float,y:Float) {
        this.x=x;
        this.y=y;
    }
}

class TilemapObject{
  public var name:String;
  public var type:String;
  public var gid:Int;
  public var x:Int;
  public var y:Int;
  public var width:Int;
  public var height:Int;
  public var visible:Bool;
  public var customAttr:Map<String, String>;
  public var shapePoints:Array<Point>;
  public var isEllipse:Bool;
  public var tile:Tile;
  public var layerName:String;
  public function new(){
    customAttr = new Map<String, String>();
  }
}

private class Objectgroup{
  public var name:String;
  public var data:Array<TilemapObject>;
  public var visible:Bool;

  public function new(){
    data = new Array<TilemapObject>();
  }
}

class Tilemap extends Sprite {

  public var mapWidth(default, null):Int;
  public var mapHeight(default, null):Int;
  public var tileWidth(default, null):Int;
  public var tileHeight(default, null):Int;
  public var orientation(default, null):Orientation;
  public var renderOrder(default, null):RenderOrder;
  private var _tiles:Array<Tile>;
  private var _layers:Array<Layer>;
  private var _objectgroups:Array<Objectgroup>;
  private var _assets:AssetManager;

  public function new(
          assets:AssetManager, 
          xml:String,
          img_suffix:String = null,
          object_callback:TilemapObject->Void = null) {
    super();
    _assets = assets;

    var xml = Xml.parse(haxe.Resource.getString(xml));
    var source = new Fast(xml.firstElement());

    var txt:String;

    txt = source.att.orientation;
    if (txt == "") {
      orientation = Orientation.Orthogonal;
    } else if (txt == "orthogonal") {
      orientation = Orientation.Orthogonal;
    } else if (txt == "isometric") {
      orientation = Orientation.Isometric;
    } else if (txt == "isometric-staggered") {
      orientation = Orientation.IsometricStaggered;
    } else if (txt == "hexagonal-staggered") {
      orientation = Orientation.HexagonalStaggered;
    }

    txt = source.att.renderorder;
    if (txt == "") {
      renderOrder = RenderOrder.RightDown;
    } else if (txt == "right-down") {
      renderOrder = RenderOrder.RightDown;
    } else if (txt == "right-up") {
      renderOrder = RenderOrder.RightUp;
    } else if (txt == "left-down") {
      renderOrder = RenderOrder.LeftDown;
    } else if (txt == "left-up") {
      renderOrder = RenderOrder.LeftUp;
    }

    mapWidth = Std.parseInt(source.att.width);
    mapHeight = Std.parseInt(source.att.height);

    tileWidth = Std.parseInt(source.att.tilewidth);
    tileHeight = Std.parseInt(source.att.tileheight);

    _tiles = new Array<Tile>();
    for (tileset in source.nodes.tileset) {

      var _tileset_tiles = new Array<Tile>();
      if (tileset.has.source) {
        throw "External tileset source not supported.";
      }
      if (tileset.has.margin || tileset.has.spacing) {
        //throw "Only image collections are supported.";
          var twidth = Std.parseInt(tileset.att.tilewidth);
          var theight = Std.parseInt(tileset.att.tileheight);
          var margin = Std.parseInt(tileset.att.margin);
          var spacing = Std.parseInt(tileset.att.spacing);
          var img = tileset.node.image;

          var htiles = Std.int((Std.parseInt(img.att.width)-margin*2+spacing)/(twidth+spacing));
          var vtiles = Std.int((Std.parseInt(img.att.height)-margin*2+spacing)/(theight+spacing));

          for(j in 0...vtiles)
              for(i in 0...htiles)
              {
                  var t = new Tile();
                  t.id = j*htiles+i;
                  t.width = twidth;
                  t.height = theight;
                  t.offset_x = margin+(twidth+spacing)*i;
                  t.offset_y = margin+(theight+spacing)*j;
                  t.source = img.att.source;
                  t.source = t.source.substr(t.source.lastIndexOf("/")+1);
                  t.source = t.source.substr(0, t.source.length-4);
              if(img_suffix != null) t.source += img_suffix;
                  _tileset_tiles.push(t);
                  _tiles.push(t);
              }

          for(tile in tileset.nodes.tile) {
              var _id = Std.parseInt(tile.att.id);
              for(prop in tile.node.properties.nodes.property){
                  _tileset_tiles[_id].properties.set(prop.att.name,prop.att.value);
              }
          }
          
      }
      else {
        for (tile in tileset.nodes.tile) {
          if (tile.has.id) {
            var t = new Tile();
            t.id = Std.parseInt(tile.att.id);
            for (image in tile.nodes.image) {
              t.width = Std.parseInt(image.att.width);
              t.height = Std.parseInt(image.att.height);
              t.source = image.att.source;
              t.source = t.source.substr(t.source.lastIndexOf("/")+1);
              t.source = t.source.substr(0, t.source.length-4);
              if(img_suffix != null) t.source += img_suffix;
            }
            //for(prop in tile.node.properties.nodes.property){
            //    t.properties.set(prop.att.name,prop.att.value);
            //}

            _tileset_tiles.push(t);
            _tiles.push(t);
          }
        }
      }
    }

    _layers = new Array<Layer>();
    for (layer in source.nodes.layer) {
      var t = new Layer();
      t.name = layer.att.name;
      t.visible = true;
      if(layer.has.visible) t.visible=false;
      for (i in 0...mapHeight) {
        t.data.push(new Array<Tile>());
        for (j in 0...mapWidth) {
          t.data[i].push(null);
        }
      }
      var i = 0;
      for (data in layer.nodes.data) {
        for (tile in data.nodes.tile) {
          t.data[Std.int(i / mapWidth)][Std.int(i % mapWidth)] = _tiles[Std.parseInt(tile.att.gid)-1];
          i += 1;
        }
      }
      _layers.push(t);
    }

    _objectgroups = new Array<Objectgroup>();
    for (objectgroup in source.nodes.objectgroup) {
      var t = new Objectgroup();
      t.name = objectgroup.att.name;
      t.visible=true;
      if(objectgroup.has.visible)visible=false;
      for(object in objectgroup.nodes.object){
        var o = new TilemapObject();
        o.gid = (object.has.gid) ? Std.parseInt(object.att.gid) : 0;
        o.x = Std.parseInt(object.att.x);
        o.y = Std.parseInt(object.att.y);
        o.name = (object.has.name)?object.att.name:"";
        o.type = (object.has.type)?object.att.type:"";
        o.width = (object.has.width)?Std.parseInt(object.att.width) : 1;
        o.height = (object.has.height)?Std.parseInt(object.att.height) : 1;
        o.visible = (object.has.visible)?false:true;
        o.layerName = objectgroup.att.name;
        o.isEllipse = (object.hasNode.ellipse) ? true : false;
        if(object.hasNode.polygon){
            o.shapePoints = new Array<Point>();
            for(_point_str in object.node.polygon.att.points.split(" ")){
                var _point_parts = _point_str.split(",");
                var _point = new Point(
                        Std.parseFloat(_point_parts[0]),
                        Std.parseFloat(_point_parts[1]));
                o.shapePoints.push(_point);
            }
        }
        
        if(object.hasNode.properties){
          for(property in object.node.properties.nodes.property){
            o.customAttr[property.att.name.toLowerCase()]=property.att.value;
          }
        }

        t.data.push(o);
      }
      _objectgroups.push(t);
    }



    for (layer in _layers) {
      if(!layer.visible) continue;
      // The default is renderOrder == RenderOrder.RightDown
      var xi = 0;
      var xf = mapWidth;
      var dx = 1;
      var yi = 0;
      var yf = mapHeight;
      var dy = 1;
      
      if (renderOrder == RenderOrder.RightUp) {
        xi = 0;
        xf = mapWidth;
        dx = 1;
        yi = mapHeight-1;
        yf = -1;
        dy = -1;
      }

      if (renderOrder == RenderOrder.LeftDown) {
        xi = mapWidth-1;
        xf = -1;
        dx = -1;
        yi = 0;
        yf = mapHeight;
        dy = 1;
      }

      if (renderOrder == RenderOrder.LeftUp) {
        xi = mapWidth-1;
        xf = -1;
        dx = -1;
        yi = mapHeight-1;
        yf = -1;
        dy = -1;
      }

      var _x = 0;
      var _y = 0;
      while (_y != yf) {
        while (_x != xf) {
          var cell = layer.data[_y][_x];
          if (cell != null && cell.source != null && cell.source != "") {
            var tex = _assets.getTexture(cell.source);
            tex = Texture.fromTexture(tex,new Rectangle(cell.offset_x,cell.offset_y,cell.width,cell.height));
            var img = new Image(tex);
            //img.pivotY = img.height;
            img.smoothing = TextureSmoothing.NONE;
            img.x = _x*tileWidth;
            img.y = _y*tileHeight;
            addChild(img);
          }
          _x += dy;
        }
        _x = xi;
        _y += dy;
      }
    }
    for(objectgroup in _objectgroups){
      if(!objectgroup.visible) continue;
      for(object in objectgroup.data){
        if(true) {
          if(object.layerName == "Decals")
          {
            var cell =_tiles[object.gid-1];
            var img = new Image(_assets.getTexture(cell.source));
            img.x = object.x;
            img.y = object.y-cell.height;
            //img.smoothing = TextureSmoothing.NONE;

            //for(key in object.customAttr.keys()){
              //key = key.toLowerCase();
            //}

            addChild(img);
          }
          else if(object_callback == null){
            //var cell =_tiles[object.gid-1];
            //var img = new Image(_assets.getTexture(cell.source.substr(cell.source.lastIndexOf('/')+1)));
            //img.x = object.x;
            //img.y = object.y;
            //img.smoothing = TextureSmoothing.NONE;

            //for(key in object.customAttr.keys()){
            //  key = key.toLowerCase();
            //}

            //addChild(img);
          } else {
              if(object.gid != 0) object.tile = _tiles[object.gid-1];
              object_callback(object);
          }
        }
      }
    }

  }

}
