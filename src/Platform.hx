import box2D.dynamics.*;
import box2D.collision.shapes.*;
import Game;

class Platform extends FixturePhysicsEntity {
	public function new(shape:B2Shape, body:B2Body, game:Game, ?n:String){
		var fixtureDef = new B2FixtureDef();
		fixtureDef.shape = shape;
		fixtureDef.filter.categoryBits = CollisionFilters.Platform;
		fixtureDef.filter.maskBits = CollisionFilters.Player | CollisionFilters.NPC | CollisionFilters.Foreground | CollisionFilters.Background;
        super(fixtureDef, body, game);
    }
}