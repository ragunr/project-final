import starling.animation.*;
import bitmasq.Gamepad;
import bitmasq.GamepadEvent;
import starling.display.*;
import box2D.common.math.*;
import Game;
import Light;
import flash.media.SoundTransform;

class MapScripts {
    public static function fireMapScript(map_name, event_name, game){

       switch(map_name) {
           case "test_map":
               switch(event_name) {
                   case "0": MapScripts.scriptTestMapEvent0(game);
               }
           case "map_1":
               switch(event_name) {
                   case "0": MapScripts.scriptMap1Event0(game); 
                   case "1": MapScripts.scriptMap1Event1(game); 
                   case "2": MapScripts.scriptMap1Event2(game); 
               }
           case "map_2":
               switch(event_name) {
                   case "0": MapScripts.scriptMap2Event0(game); 
                   case "1": MapScripts.scriptMap2Event1(game); 
               }
			case "map_3": 
                switch(event_name) { 
                    case "0": MapScripts.scriptMap3Event0(game); 
                    case "1": MapScripts.scriptMap3Event1(game); 
                    case "2": MapScripts.scriptMap3Event2(game); 
                    case "Vent": MapScripts.scriptMap3EventVent(game); 
                    case "Incinerate": MapScripts.scriptMap3EventIncinerate(game); 
                    case "Passage": MapScripts.scriptMap3EventPassage(game); 
                    case "SaveTeen": MapScripts.scriptMap3EventSaveTeen(game); 
                    case "TeenFlee": MapScripts.scriptMap3EventTeenFlee(game); 
                } 

                case "map_4": 
                switch(event_name) { 
                    case "0": MapScripts.scriptMap4Event0(game); 
                    case "1": MapScripts.scriptMap4Event1(game); 
                    case "2": MapScripts.scriptMap4Event2(game); 
                    case "3": MapScripts.scriptMap4Event3(game); 
                    case "4": MapScripts.scriptMap4Event4(game); 
                } 

			case "map_5":
				switch(event_name) {
					case "0": MapScripts.scriptMap5Event0(game);
					case "safe_trigger_2": MapScripts.scriptMap5Event1(game);
					case "safe_trigger_3": MapScripts.scriptMap5Event2(game);
				}
				
			case "map_7":
				switch(event_name) {
					case "map_7_exit_trigger": MapScripts.scriptMap7Event2(game);
				}
       
		}
		
	}

    public static function firePodBurst(game:Game,x:Float,y:Float){
        var TIME_OPEN = 0.1;
        var TIME_CLOSE = 0.3;

        var burst_img = new Image(Root.assets.getTexture("hall-pods-burst"));
        burst_img.x = x;
        burst_img.y = y-118;
        burst_img.blendMode = BlendMode.SCREEN;
        burst_img.alpha = 0;
        game.world_lit.addChild(burst_img);

        var burst_img_dark = new Image(Root.assets.getTexture("hall-pods-burst"));
        burst_img_dark.x = x;
        burst_img_dark.y = y-118;
        burst_img_dark.blendMode = BlendMode.SCREEN;
        burst_img_dark.alpha = 0;
        game.world_dark.addChild(burst_img_dark);
       
        Root.assets.playSound("scratch1",900,0,new SoundTransform(0.4));

        game.juggler.tween(burst_img, TIME_OPEN, {
            transition: Transitions.EASE_IN,
            alpha : 0.9
            });
        game.juggler.tween(burst_img, TIME_CLOSE, {
            transition: Transitions.EASE_OUT,
            alpha : 0,
            delay : TIME_OPEN
            });

        game.juggler.tween(burst_img_dark, TIME_OPEN, {
            transition: Transitions.EASE_IN,
            alpha : 0.9
            });
        game.juggler.tween(burst_img_dark, TIME_CLOSE, {
            transition: Transitions.EASE_OUT,
            alpha : 0,
            delay : TIME_OPEN
            });

        game.juggler.delayCall(function(){
            game.world_lit.removeChild(burst_img);
            game.world_dark.removeChild(burst_img_dark);
            }, TIME_OPEN+TIME_CLOSE);
    }

    public static function scriptTestMapEvent0(game:Game){
        var slider = cast(game.entities_by_name["slider"],PhysicsEntity);

        var MOVE_TIME : Float = 4.66;
        var loop = new DelayedCall(null,MOVE_TIME*2);
        var pattern = function(dcDelayedCall) {
            slider.juggler.delayCall( function () {
                slider.body.setLinearVelocity(new B2Vec2(1.5,0));
                }, 0);
            slider.juggler.delayCall( function () {
                slider.body.setLinearVelocity(new B2Vec2(-1.5,0));
                }, MOVE_TIME);
        }
        loop.reset(pattern,MOVE_TIME*2,[loop]);
        loop.repeatCount = 0;
        loop.advanceTime(MOVE_TIME*2);
        slider.juggler.add(loop);

        var burst_loop = new DelayedCall(firePodBurst,2.0,[game,1332.00,658.00]); 
        burst_loop.repeatCount = 0;
        game.juggler.add(burst_loop);

    }
    public static function scriptTestMapEvent1(game:Game)
    {
        var slider = cast(game.entities_by_name["slider"], PhysicsEntity);
        slider.juggler.delayCall(function () 
                {
                    slider.body.setLinearVelocity(new B2Vec2(0, 1));
                }, 0);
        slider.juggler.delayCall(function () 
                {
                    slider.body.setLinearVelocity(new B2Vec2(0, 0));
                }, 5);
    }

    public static function scriptMap1Event0(game:Game){
        game.player.flashlightEnabled = false;
        game.player.runEnabled = false;
        game.player.toggleFlashlight();
    }

    public static function scriptMap1Event1(game:Game)
    {
		game.player.setMask(CollisionFilters.Platform | 
                CollisionFilters.Foreground |
                CollisionFilters.NPC);

        var attendant1:NPC;
        var attendant2:NPC;

        untyped {
            attendant1 = game.entities_by_name["attendant1"];
            attendant2 = game.entities_by_name["attendant2"];
        }

        game.playScene("triggerT2", function(){
                attendant1.setMask( CollisionFilters.Player);
                attendant1.body.setType(KINEMATIC_BODY);
                attendant1.move(Left);
                        
                attendant2.setMask( CollisionFilters.Player);
                attendant2.body.setType(KINEMATIC_BODY);
                attendant2.move(Left);

                game.juggler.delayCall(function(){
                    attendant1.faceForward = !attendant1.faceForward;
                    attendant1.stopMoving();
                    attendant2.stopMoving();
                    game.playScene("triggerT3");
                    },39,[game]);

                game.juggler.delayCall(function(){
                    game.playScene("triggerT4");
                    },49,[game]);

                game.juggler.delayCall(function(){
                    game.playScene("triggerT5");
                    },59,[game]);
                
                });
                
    }

    public static function scriptMap1Event2(game:Game){
			Root.get().nextLevel("map_2");
    }

    public static function scriptMap2Event0(game:Game){
        game.player.flashlightEnabled = false;
        game.player.runEnabled = false;
        game.player.toggleFlashlight();
        game.player.move(Left);
        game.player.stopMoving();

        game.playScene("dream_0");

        var poll = new DelayedCall(null, 1.0);
        poll.reset(
                function()
                {
                    if(!game.player.isIlluminated()){
                        scriptMap2Event2(game);
                        game.juggler.remove(poll);
                    }
                },1.0);
        poll.repeatCount = 0;
        game.juggler.add(poll);

        var monster : NPC = cast(game.entities_by_name["monster"]);
        monster.sprite_lit.scaleX = -1;
        monster.sprite_dark.scaleX = -1;
        monster.speed = 200;

    }

    public static function scriptMap2Event1(game:Game){
        var light = cast(game.entities_by_name["hall_light"],Light);
        for(i in [0,3]){
            var vert = light.polygon.getVertex(i);
            var tween = new Tween(vert, 32, Transitions.EASE_IN);
            tween.animate("x",128*Game.PPM);
            tween.onUpdate = function(){
                light.polygon.setVertex(i,vert.x,vert.y);
            }
            light.juggler.add(tween);
        }
        var monster : NPC = cast(game.entities_by_name["monster"]);
        monster.move(Right);
        
    }

    public static function scriptMap2Event2(game:Game){
        game.sounds.playEffect(growl1);
        game.sounds.playEffect(growl2);
        var monster : NPC = cast(game.entities_by_name["monster"]);
        monster.stopMoving();
        monster.playAnimation("talk");
        game.juggler.delayCall(function(){
                    Root.get().nextLevel("map_3");
                },1.0);
    }

    public static function scriptMap3Event0(game:Game) {
        //game.player.flashlightEnabled = false;
        game.player.toggleFlashlight();
        game.playScene("3_0");
        untyped{
        game.entities_by_name["clerk_dead"].setVisible(false);
        game.entities_by_name["teen_flee_trigger"].beginContactCallback = null;
        }
    }

    public static function scriptMap3EventVent(game:Game) {
        game.playScene("3_Vent",function(){
                Gamepad.get().pushContext();
                game.player.playAnimation("pullup");
                game.player.body.setActive(false);
                
                var target = {x:game.player.position.x,y:game.player.position.y};
                var jumptween = new Tween(target,5.0/14.0);
                jumptween.animate("y", target.y-82);
                jumptween.onUpdate = function(){game.player.teleportTo(target);};
                game.juggler.add(jumptween);

                game.juggler.delayCall(function() {
                    game.player.playAnimation("pullup");
                    game.player.pauseAnimation();
                    game.player.teleportTo({x:3362.00,y:2950.00});
                    Gamepad.get().popContext();
                    game.player.body.setActive(true);
                    },0.99);
                });
    }

    

    public static function scriptMap3EventIncinerate(game:Game) {
        Gamepad.get().pushContext();
        game.player.stopMoving();

        var st = Root.get().musicChannel.soundTransform;
        var volume_tween = new Tween(st, 2.0);
        volume_tween.animate("volume",0);
        volume_tween.onUpdate = function(){Root.get().musicChannel.soundTransform = st;};
        game.juggler.add(volume_tween);

        var start_x = 1044.00;
        var start_y = 1295.00+10;
        for(i in 0...33){
            var x = start_x + 72*(3.0/4)*i;
            var y = start_y - 108*((i)%2);
            game.juggler.delayCall(firePodBurst,2+0.15*i,game,x,y);
        }
        game.juggler.delayCall(function() {
                game.playScene("3_Incinerate",function(){
                    var volume_tween = new Tween(st, 2.0);
                    volume_tween.animate("volume",0.1);
                    volume_tween.onUpdate = function(){Root.get().musicChannel.soundTransform = st;};
                    game.juggler.add(volume_tween);
                    Gamepad.get().popContext();
                    });
                }, 2+0.15*34);
    }
    

    public static function scriptMap3EventPassage(game:Game) {
        game.playScene("3_Passage",function(){
                game.player.teleport({x:-10,y:864});
                });
        
    }

    public static function scriptMap3EventSaveTeen(game:Game) {

        var st = Root.get().musicChannel.soundTransform;
        var volume_tween = new Tween(st, 2.0);
        volume_tween.animate("volume",0);
        volume_tween.onUpdate = function(){Root.get().musicChannel.soundTransform = st;};
        game.juggler.add(volume_tween);

        var start_x = 2772.00;
        var start_y = 2303.00+10;
        game.juggler.delayCall(function() {
                }, 2+0.15*34);
        var animations = {
            stand: "teenstanding", 
            walk: "teenagerwalk",
            talk: "buisnesstalking"
        }
        var teen : NPC;
        game.playScene("3_SaveTeen1", function() {
                
                Gamepad.get().pushContext();
                for(i in 0...33){
                    var x = start_x - 72*(3.0/4)*i;
                    var y = start_y - 108*((i)%2);
                    game.juggler.delayCall(firePodBurst,0.15*i,game,x,y);
                }
                game.player.playAnimation("save_teen");
                untyped { game.entities_by_name["teen-pod"].setVisible(false); }
                game.juggler.delayCall(function(){
                    teen = new NPC(
                        new B2Vec2(2260,2160),
                        animations,
                        0x222222,
                        game);
                    teen.name = "teen";
                    game.entities.push(teen);
                    game.entities_by_name[teen.name] = teen;
                    teen.move(Left);
                    teen.stopMoving();

                    game.player.playAnimation("walk");
                    game.player.pauseAnimation();

                    }, 0.65);
                });
                game.juggler.delayCall(function(){
                    untyped{
                        game.entities_by_name["door_a"].scriptOpenDoor();
                        game.entities_by_name["door_b"].scriptOpenDoor();
                        game.entities_by_name["door_c"].scriptOpenDoor();
                        game.entities_by_name["door_d"].scriptOpenDoor();
                        game.entities_by_name["clerk"].setVisible(false);
                        game.entities_by_name["clerk_dead"].setVisible(true);
                        game.entities_by_name["clerk_trigger"].beginContactCallback = null;
                        game.entities_by_name["teen_flee_trigger"].scriptFireMapEvent("TeenFlee");
                        game.entities_by_name["loby_light_1"].switch_off();
                        game.entities_by_name["loby_light_2"].switch_off();
                        for(i in 1...10){
                            game.entities_by_name["flicker_"+i+"_1"].scriptLightFlicker(i);
                            game.entities_by_name["flicker_"+i+"_2"].scriptLightFlicker(i);
                        }
                    }
                    Root.assets.playSound("neckCrack",0,0,new SoundTransform(1));
                    Root.assets.playSound("smash1",0,0,new SoundTransform(1));
                    Root.assets.playSound("metalDrop1",0,0,new SoundTransform(1));
                    Root.assets.playSound("scratch1",0,0,new SoundTransform(3));
                    Root.assets.playSound("scratch1",200,0,new SoundTransform(3));

                    game.playScene("3_SaveTeen2",function(){
                        teen.speed = 350;
                        teen.move(Left);
                        game.juggler.delayCall(function(){
                            Gamepad.get().popContext();
                            teen.teleportTo({x:6494.00,y:3312.00});
                            teen.stopMoving();
                            var volume_tween = new Tween(st, 2.0);
                            volume_tween.animate("volume",0.1);
                            volume_tween.onUpdate = function(){Root.get().musicChannel.soundTransform = st;};
                            game.juggler.add(volume_tween);

                            },4);
                        });
                    },0.15*34+1);
    }

    public static function scriptMap3EventTeenFlee(game:Game) {
        game.playScene("3_SaveTeen3");
        var teen : NPC = cast(game.entities_by_name["teen"]);
        teen.move(Right);
    }

    public static function scriptMap3Event1(game:Game){
        var event2trigger = cast(game.entities_by_name["event_2_trigger"]);
        event2trigger.scriptFireMapEvent("2");
        var first_jump_trigger = cast(game.entities_by_name["first_jump"]);
        first_jump_trigger.beginContactCallback = null;
    }

    public static function scriptMap3Event2(game:Game){
        Root.get().nextLevel("map_4");
    }

    public static function scriptMap4Event0(game:Game) {
        game.player.flashlightEnabled = false;
        game.player.toggleFlashlight();
        var poll = new DelayedCall(null, 1.0);
        poll.reset(
                function()
                {
                    if(!game.player.isIlluminated()){
                        game.juggler.remove(poll);
                    }
                },1.0);
        poll.repeatCount = 0;
        game.juggler.add(poll);
        game.playScene("4_0");
    }

    public static function scriptMap4Event1(game:Game) {
        var light: Light = cast(game.entities_by_name["Light1"],Light);
        game.lights.remove(light.polygon);

        light= cast(game.entities_by_name["Light2"],Light);
        game.lights.remove(light.polygon);

        light = cast(game.entities_by_name["Light3"],Light);
        game.lights.remove(light.polygon);

        light= cast(game.entities_by_name["Light4"],Light);
        game.lights.remove(light.polygon);

        game.sounds.playEffect(growl1);
    }

    public static function scriptMap4Event2(game:Game) {

        var light: Light = cast(game.entities_by_name["Light1"],Light);
        game.lights.add(light.polygon);

        light= cast(game.entities_by_name["Light2"],Light);
        game.lights.add(light.polygon);

        light = cast(game.entities_by_name["Light3"],Light);
        game.lights.add(light.polygon);

        light= cast(game.entities_by_name["Light4"],Light);
        game.lights.add(light.polygon);
    }

    public static function scriptMap4Event3(game:Game) {
        game.sounds.playEffect(growl3);

    }

    public static function scriptMap4Event4(game:Game) {
        game.sounds.playEffect(growl2);

    }
	
	public static function scriptMap5Event0(game:Game) {
		var bellhop = cast(game.entities_by_name["bellhop"], NPC);
		bellhop.move(Left);
		bellhop.stopMoving();
		game.player.flashlightEnabled = false;
		game.player.toggleFlashlight();
	}
	
	public static function scriptMap5Event1(game:Game) {
		var business = cast(game.entities_by_name["business"], NPC);
		business.move(Left);
		business.stopMoving();
		game.playScene("safe_trigger_2");
		game.player.flashlightEnabled = true;
	}
	
	public static function scriptMap5Event2(game:Game) {
		var business = cast(game.entities_by_name["business"], NPC);
		business.move(Right);
		business.stopMoving();
		var bellhop = cast(game.entities_by_name["bellhop"], NPC);
		bellhop.move(Right);
		bellhop.stopMoving();
		game.playScene("safe_trigger_3");
		game.juggler.delayCall(function() {
			game.player.toggleFlashlight();
			// TO-DO change this to map_6
			Root.get().nextLevel("map_1");
		}, 1.5);
		
	}
	
	public static function scriptMap7Event2(game:Game) {
		Root.get().nextLevel("map_8");
	}


}

