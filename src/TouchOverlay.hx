import starling.core.Starling;
import bitmasq.Gamepad;
import bitmasq.GamepadEvent;
import starling.display.*;
import starling.animation.*;
import starling.events.*;
import starling.textures.*;
import starling.geom.Polygon;
import flash.geom.Point;
import flash.ui.*;
import box2D.dynamics.*;
import box2D.dynamics.contacts.*;
import box2D.common.math.*;
import box2D.collision.*;
import box2D.collision.shapes.*;
import flash.events.KeyboardEvent;
import Geom;
import Tilemap;
import DialogueController;
import Light;
import Trigger;
import Platform;
import SoundEffects;
using Geom.Vec2Ext;
using Geom.RectExt;
using Lambda;
using Util;


class TouchOverlay extends Sprite
{
    public var leftTouch:Quad;
    public var rightTouch:Quad;
    public var flashlight:Image;
    public var sprint:Image;
    public var use:Image;

    public function new()
    {
        super();
        //setting these up to use later
        var w = Root.STAGE_WIDTH;
        var h = Root.STAGE_HEIGHT;

        //left touch input
        leftTouch = new Quad(w/10, h, 0x0000ff);
        leftTouch.alpha = .20;

        //right touch input
        rightTouch = new Quad(w/10, h, 0x0000ff);
        rightTouch.alpha = .20;
        rightTouch.x = (w - (w/10));

        //the flashlight button to toggle the flashlight on and off
        flashlight = new Image(Root.assets.getTexture("betterFlashlightForTouch"));
        //flashlight = new Quad(w/8, h/16, 0x0000ff);
        //flashlight.alpha = .50;
        flashlight.x = (w/2 + flashlight.width);
        flashlight.y = (h - flashlight.height);

        //scaled it a little bit because it seemed a little bit too big next to the sprint button
        flashlight.scaleY = 0.9;
        flashlight.scaleX = 0.9;

        //the sprint button to toggle sprinting on and off... hopefully
        sprint = new Image(Root.assets.getTexture("sprintForTouch"));
        sprint.x = (w/2 - sprint.width);
        sprint.y = (h - sprint.height);

        //the use button to interact with out surroundings
        use = new Image(Root.assets.getTexture("useForTouch"));
        use.x = (w/2 - sprint.width - use.width * 4);
        use.y = (h - use.height);

        //adding the children
        addChild(leftTouch);
        addChild(rightTouch);
        addChild(flashlight);
        addChild(sprint);
        addChild(use);

        //this maps the actual touch events to key events so that we can use it with the game controller as well
        keyMap = new Map<DisplayObject, Int>();
        keyMap[leftTouch] = Keyboard.LEFT;
        keyMap[rightTouch] = Keyboard.RIGHT;
        keyMap[flashlight] = Keyboard.NUMPAD_2;
        keyMap[use] = Keyboard.SPACE;
        for (button in keyMap.keys())
        {
            button.addEventListener("touch", touchListener);
        }
    }

    public var keyMap:Map<DisplayObject, Int>;

    public function touchListener(e:TouchEvent)
    { 
        var touches /*:Vector<Touch>*/ = e.getTouches(this);
        
        for(touch in touches)
        {
            var keyType = null;
            if (touch.phase == TouchPhase.BEGAN)
            {
                //trace("touchListener");
                keyType = KeyboardEvent.KEY_DOWN;
            }
            else if (touch.phase == TouchPhase.ENDED)
            {
                keyType = KeyboardEvent.KEY_UP;
            }
            else continue;

            //trace(touch.target);
            if (!keyMap.exists(touch.target)) continue;
            var keyCode = keyMap[touch.target];
            //trace(keyCode);
            var keyEvent = new KeyboardEvent(keyType, true, false, 0, keyCode);
            //keyEvent.keyCode = keyCode;
            //Root.get().dispatchEvent(keyEvent);
            flash.Lib.current.stage.dispatchEvent(keyEvent);
            //Starling.current.stage.dispatchEvent(keyEvent);
            //Gamepad.get().dispatchEvent(keyEvent);
        }
    }
}
