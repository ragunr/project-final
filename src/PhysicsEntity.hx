import Game;
import box2D.dynamics.*;
import box2D.common.math.*;
import box2D.collision.shapes.*;
import starling.display.*;
import Geom;
using Geom.Vec2Ext;

class PhysicsEntity extends SpriteEntity {
	public var body : B2Body;
	
	private var colliderLines : Array<Quad>;
	
	private var category : Int;
	private var mask : Int;
    private var fixtures : Array<B2Fixture>;

    public function new(position:B2Vec2, sprite_lit:AnimSprite, sprite_dark:AnimSprite, bodyDef:B2BodyDef, game:Game){
		super(position, sprite_lit, sprite_dark, game);
        fixtures = new Array<B2Fixture>();
		sprite_lit.alignPivot();
		sprite_dark.alignPivot();
		bodyDef.position.set((position.x + sprite_lit.width / 2) / Game.PPM, (position.y + sprite_lit.height / 2) / Game.PPM);
		body = game.world_phys.createBody(bodyDef);
		colliderLines = new Array<Quad>();
    }
	
	public function setCategory(category:Int) {
		this.category = category;
        for(fixture in fixtures){
            var filter_data = fixture.getFilterData();
            filter_data.categoryBits = category;
            fixture.setFilterData(filter_data);
        }
	}
	
	public function setMask(mask:Int) {
		this.mask = mask;
        for(fixture in fixtures){
            var filter_data = fixture.getFilterData();
            filter_data.maskBits = mask;
            fixture.setFilterData(filter_data);
        }
	}
	
	public function createFixture(fixtureDef:B2FixtureDef) {
		fixtureDef.filter.categoryBits = category;
		fixtureDef.filter.maskBits = mask;
		var fix = body.createFixture(fixtureDef);
        fix.SetUserData(this);
        fixtures.push(fix);
	}
	
	public function applyImpulse(direction:B2Vec2) {
		direction.x /= Game.PPM;
		direction.y /= Game.PPM;
		body.applyImpulse(direction, body.getWorldCenter());
	}

	public function applyForce(direction:B2Vec2) {
		
		body.applyForce(direction, body.getWorldCenter());

	}
	
	public function setLinearVelocity(direction:B2Vec2) {
		direction.x /= Game.PPM;
		direction.y /= Game.PPM;
		var velocity = body.getLinearVelocity();
		direction.y += velocity.y;
		body.setLinearVelocity(direction);
	}
	
	public override function advanceTime(t:Float){
        if(target != null) {
            body.setPosition(target);
            target = null;
        }
		synchronize();
        super.advanceTime(t);
    }
	
	public override function dispose() {
		super.dispose();
		game.world_phys.destroyBody(body);
	}

    var target : B2Vec2 = null;
    override function teleport(offset:Vec2){
        var pos = body.getPosition();
        pos = new B2Vec2(pos.x, pos.y);
        pos.x += offset.x/Game.PPM;
        pos.y += offset.y/Game.PPM;
        target = pos;
        super.teleport(offset);
    }
	
    override function teleportTo(t:Vec2){
        var pos = body.getPosition();
        pos = new B2Vec2(pos.x, pos.y);
        pos.x = t.x/Game.PPM;
        pos.y = t.y/Game.PPM;
        target = pos;
        super.teleport(t);
    }
	
	public override function drawCollider() {
		var fixtures = body.getFixtureList();
		if (fixtures != null) {
			var shapeType = fixtures.getType();
			if (shapeType == POLYGON_SHAPE) {
				var polygonShape = cast(fixtures.getShape(), B2PolygonShape);
				var vertices = polygonShape.getVertices();
				if (vertices.length > 2) {
					for (i in 0...vertices.length - 1) {
						drawColliderSegment(vertices[i], vertices[i+1], i);
					}
					drawColliderSegment(vertices[vertices.length - 1], vertices[0], vertices.length - 1);
				}
			}
		}
	}
	
	private function drawColliderSegment(vert1:B2Vec2, vert2:B2Vec2, i:Int) {
		var pos1 = new B2Vec2((body.getPosition().x + vert1.x), (body.getPosition().y + vert1.y));
		var pos2 = new B2Vec2((body.getPosition().x + vert2.x), (body.getPosition().y + vert2.y));
		if (body.getAngle() != 0) {
			pos1 = Util.rotateAroundPoint(pos1, body.getPosition(), body.getAngle());
			pos2 = Util.rotateAroundPoint(pos2, body.getPosition(), body.getAngle());
		}
		pos1.x *= Game.PPM;
		pos1.y *= Game.PPM;
		pos2.x *= Game.PPM;
		pos2.y *= Game.PPM;
		var quad = Util.createLine(pos1, pos2);
		colliderLines[i] = quad;
		game.debugLayer.addChild(quad);
	}
	
	public override function deleteCollider() {
		for (i in 0...colliderLines.length) {
			game.debugLayer.removeChild(colliderLines[i]);
		}
	}
	
	private function synchronize() {
		var b2position : B2Vec2 = body.getPosition();
        position = {x:b2position.x, y:b2position.y}.mult(Game.PPM);
        position.apply(sprite_lit);
        position.apply(sprite_dark);
		
		var rotation : Float = body.getAngle();
		sprite_lit.rotation = rotation;
		sprite_dark.rotation = rotation;
	}
    override function fireScript(script_name:String, params:Array<String>) {
        switch(script_name){
            case "openDoor": scriptOpenDoor();
            default: super.fireScript(script_name,params);
        }
    }

    public function scriptOpenDoor() {
        body.setActive(false);
        sprite_lit.removeChildren(0,-1,true);
        sprite_lit.addChild(new Image(Root.assets.getTexture("door2")));
        sprite_dark.removeChildren(0,-1,true);
        sprite_dark.addChild(new Image(Root.assets.getTexture("door2_dark")));
    }

    public function scriptCloseDoor() {
        body.setActive(true);
        sprite_lit.removeChildren(0,-1,true);
        sprite_lit.addChild(new Image(Root.assets.getTexture("door1")));
        sprite_dark.removeChildren(0,-1,true);
        sprite_dark.addChild(new Image(Root.assets.getTexture("door1_dark")));
    }
    
}
