import Tilemap;
import flash.geom.Point;
import flash.geom.Rectangle;
import starling.geom.Polygon;
import starling.display.Quad;
import box2D.common.math.*;
import box2D.collision.shapes.B2Shape;
import box2D.collision.shapes.B2CircleShape;
import box2D.collision.shapes.B2PolygonShape;
import Math;

class Util{
    public static function buildStarlingPolygon(
            obj:TilemapObject
            ):Polygon {
        var poly : Polygon;
        // if we have shape points, interpret as polygon
        if(obj.shapePoints != null){
            var point_list = new Array<Point>();
            for(p in obj.shapePoints){
                point_list.push(new Point(obj.x+p.x,obj.y+p.y));
            }
            poly = new Polygon(point_list);
        } else if(obj.isEllipse) {
            poly = Polygon.createEllipse(
	    	obj.x+obj.width/2,
	    	obj.y+obj.height/2,
	    	obj.width/2,
	    	obj.height/2);
	    // otherwise, assume its a rectangle
	    } else {
            poly = Polygon.createRectangle(obj.x,obj.y,obj.width,obj.height);
        }
        return poly;
    }
    public static function buildB2Shape(
            obj:TilemapObject,
            scale:Float = null,
            centered:Bool = false
            ):B2Shape {
        if(scale == null) scale = 1;
        var poly : B2Shape;
        // if we have shape points, interpret as polygon
        if(obj.shapePoints != null){
            var point_list = new Array<B2Vec2>();
            for(i in 0...obj.shapePoints.length){
				//points must be added in counterclockwise order for collision to work
                point_list.push(new B2Vec2((obj.x+obj.shapePoints[obj.shapePoints.length-1-i].x)*scale,(obj.y+obj.shapePoints[obj.shapePoints.length-1-i].y)*scale));
            }
            poly = B2PolygonShape.asVector(point_list,point_list.length);
        } else if(obj.isEllipse) {
            if(obj.width != obj.height) throw "Can not create a B2Shape from an elipse besides perfect circles.";
            poly = new B2CircleShape( obj.width/2*scale );
	    // otherwise, assume its a rectangle
	    } else {
                if(centered){
                    poly = B2PolygonShape.asBox( obj.width/2*scale, obj.height/2*scale);
                } else {
                    poly = B2PolygonShape.asOrientedBox(
                            obj.width/2*scale,
                            obj.height/2*scale,
                            new B2Vec2((obj.x+obj.width/2)*scale,(obj.y+obj.height/2)*scale));
                }
        }
        return poly;
    }
	
	public static function createLine(vec1:B2Vec2, vec2:B2Vec2, color = 0xffffff) : Quad {
		var quad = new Quad(B2Math.distance(vec1, vec2), 2, color);
		
		var distX = vec2.x - vec1.x;
		var distY = vec2.y - vec1.y;
		
		var angle = Math.atan2(distY, distX);
		
		quad.x = vec1.x;
		quad.y = vec1.y;
		quad.rotation = angle;
		
		return quad;
	}
	
	public static function rotateAroundPoint(point:B2Vec2, pivot:B2Vec2, angle:Float) {
		var newPoint = new B2Vec2();
		
		newPoint.x = Math.cos(angle) * (point.x-pivot.x) - Math.sin(angle) * (point.y-pivot.y) + pivot.x;
		newPoint.y = Math.sin(angle) * (point.x-pivot.x) + Math.cos(angle) * (point.y-pivot.y) + pivot.y;
		
		return newPoint;
	}

        public static function getPolygonBounds(polygon:Polygon):Rectangle{
            var min_x:Float = 99999;
            var max_x:Float = -99999;
            var min_y:Float = 99999;
            var max_y:Float = -99999;
            for(i in 0...polygon.numVertices){
                var vert = polygon.getVertex(i);
                min_x = Math.min(min_x,vert.x);
                max_x = Math.max(max_x,vert.x);
                min_y = Math.min(min_y,vert.y);
                max_y = Math.max(max_y,vert.y);
            }
            return new Rectangle(min_x,min_y,max_x-min_x,max_y-min_y);
        }
}
