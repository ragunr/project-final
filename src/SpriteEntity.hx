import starling.display.*;
import Game;
import box2D.common.math.*;
import flash.geom.Point;
import Geom;
using Geom.Vec2Ext;

//Used only for static entities with no physics
class SpriteEntity extends Entity {
	public var sprite_lit : AnimSprite;
	public var sprite_dark : AnimSprite;
        public var position : Vec2;

    public function new(position:B2Vec2, sprite_lit:AnimSprite, sprite_dark:AnimSprite, game:Game){
		super(game);
		
		this.sprite_lit = sprite_lit;
		this.sprite_lit.x = position.x;
		this.sprite_lit.y = position.y;
		game.world_lit.addChild(this.sprite_lit);
		
		this.sprite_dark = sprite_dark;
		this.sprite_dark.x = position.x;
		this.sprite_dark.y = position.y;
		game.world_dark.addChild(this.sprite_dark);

                this.position = {x:position.x,y:position.y};
    }

    public function playAnimation(name:String,?scale:Vec2){
        sprite_lit.playAnimation(name,scale);
        sprite_dark.playAnimation(name,scale);
    }

    public function pauseAnimation(){
        sprite_lit.pauseAnimation();
        sprite_dark.pauseAnimation();
    }

    public function setVisible(visible:Bool){
        sprite_lit.visible = visible;
        sprite_dark.visible = visible;
    }

    override function advanceTime(t:Float){
        super.advanceTime(t);
        sprite_lit.advanceTime(t);
        sprite_dark.advanceTime(t);
    }

    public function isIlluminated():Bool{
        var mask = game.world_lit.mask;
        if(mask == null) return false;
        return mask.hitTest(new Point(position.x,position.y)) != null;
    }

    public function teleport(offset:Vec2){
        position = position.add(offset);
		sprite_lit.x = position.x;
		sprite_lit.y = position.y;
		sprite_dark.x = position.x;
		sprite_dark.y = position.y;
    }

    public function teleportTo(t:Vec2){
        position = t;
		sprite_lit.x = position.x;
		sprite_lit.y = position.y;
		sprite_dark.x = position.x;
		sprite_dark.y = position.y;
    }
	
	public override function dispose() {
		sprite_lit.dispose();
		sprite_dark.dispose();
	}
}
