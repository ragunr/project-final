
import bitmasq.Gamepad;
import starling.display.Sprite;
import starling.utils.AssetManager;
import starling.display.Image;
import starling.core.Starling;
import starling.animation.Transitions;
import flash.utils.Dictionary;
import flash.ui.Keyboard;
import flash.media.SoundChannel;
import flash.media.SoundTransform;
import Game;
import Menu;


class Root extends Sprite {

    public static var singleton : Root;

    public var musicChannel: SoundChannel;
    var transform:SoundTransform;

    public static var assets:AssetManager;

    public static inline var STAGE_WIDTH = 1920;
    public static inline var STAGE_HEIGHT = 1080;

    public function new() {
        Gamepad.init(flash.Lib.current.stage);
        super();
    }

    public static function get(){
        return singleton;
    }
    public function initialize(startup:Startup) {
        singleton = this;
        var keymap = new Dictionary();
        untyped {
            keymap[Keyboard.LEFT] = Gamepad.D_LEFT;
            keymap[Keyboard.RIGHT] = Gamepad.D_RIGHT;
            keymap[Keyboard.UP] = Gamepad.D_UP;
            keymap[Keyboard.DOWN] = Gamepad.D_DOWN;
            keymap[Keyboard.NUMPAD_1] = Gamepad.A_DOWN;
            keymap[Keyboard.NUMPAD_2] = Gamepad.A_RIGHT;
            keymap[Keyboard.NUMPAD_3] = Gamepad.A_UP;
            keymap[Keyboard.NUMPAD_4] = Gamepad.A_LEFT;
			keymap[Keyboard.SPACE] = Gamepad.A_DOWN;
        }
        Gamepad.get().addKeyController(keymap);

        assets = new AssetManager();
        // enqueue here
        assets.enqueue("src_assets/titlefont.png", "src_assets/titlefont.fnt");
        assets.enqueue("src_assets/japanese.fnt", "src_assets/japanese_0.png");
        assets.enqueue("src_assets/new.png", "src_assets/new.fnt");
        assets.enqueue("assets/flashlightForTouch.png");
        assets.enqueue("assets/betterFlashlightForTouch.png");
        assets.enqueue("assets/sprintForTouch.png");
        assets.enqueue("assets/useForTouch.png");
	assets.enqueue("src_assets/blacktie.png", "src_assets/blacktie.fnt"); // not using this yet... kind of a sketchy font
        assets.enqueue("assets/assets.png");
        assets.enqueue("assets/assets.xml");
        assets.enqueue("assets/titlescreen.png");
		
		assets.enqueue("src_assets/tilemap/chair.png", "src_assets/tilemap/chair_dark.png", "src_assets/tilemap/plant1.png", "src_assets/tilemap/plant1_dark.png");
		assets.enqueue("src_assets/tilemap/plant2.png", "src_assets/tilemap/plant2_dark.png", "src_assets/tilemap/smallLight.png", "src_assets/tilemap/smallLight_dark.png");
		assets.enqueue("src_assets/tilemap/window1.png", "src_assets/tilemap/window2.png", "src_assets/tilemap/window1_dark.png", "src_assets/tilemap/window2_dark.png");
        assets.enqueue("src_assets/Sounds/bite1.mp3");
        assets.enqueue("src_assets/Sounds/breathing.mp3");
        assets.enqueue("src_assets/Sounds/growl1.mp3");
        assets.enqueue("src_assets/Sounds/growl2.mp3");
        assets.enqueue("src_assets/Sounds/growl3.mp3");
        assets.enqueue("src_assets/Sounds/growl4.mp3");
        assets.enqueue("src_assets/Sounds/heart1.mp3");
        assets.enqueue("src_assets/Sounds/hit1.mp3");
        assets.enqueue("src_assets/Sounds/laugh1.mp3");
        assets.enqueue("src_assets/Sounds/neckCrack.mp3");
        assets.enqueue("src_assets/Sounds/smash1.mp3");
        assets.enqueue("src_assets/Sounds/squish1.mp3");
        assets.enqueue("src_assets/Sounds/stab1.mp3");
        assets.enqueue("src_assets/Sounds/thud1.mp3");
        assets.enqueue("src_assets/Sounds/walking1.mp3");
        assets.enqueue("src_assets/Sounds/walking2.mp3");
        assets.enqueue("assets/Sounds/ObjectSounds/scratch1.mp3");

        assets.enqueue("src_assets/Sounds/GameMusic1.mp3");
        assets.enqueue("src_assets/Sounds/metalDrop1.mp3");

        assets.loadQueue(function onProgress(ratio:Float) {
            if(ratio == 1) {
                Starling.juggler.tween(startup.loadingBitmap,
                    1.0,
                    {
                        transition: Transitions.EASE_OUT,
                        alpha: 0,
                        onComplete: function()
                        {
                            startup.removeChild(startup.loadingBitmap);
							start();
                        }
                    });
            }
        });
    }

    public function start(){
        // Menus should superceede start game
        startMenu();
        //startGame();

        musicChannel = assets.playSound("GameMusic1",0,100);
        transform = musicChannel.soundTransform;
        transform.volume = .5;
        musicChannel.soundTransform = transform;
    }

    public function startMenu()
    {
        var menu = new Menu();
        addChild(menu);
        //Starling.juggler.add(menu);
    }

    public function startGame(?map_name:String){
        if(map_name == null) map_name = "map_1";
        removeChildren(0,-1,true);
        Starling.juggler.purge();
        var game = new Game(map_name); 
        //var game = new Game("map_6"); //This is just for testing
        addChild(game);
        Starling.juggler.add(game);
        //var sounds = new SoundEffects();
    }

    public function nextLevel(map_name:String){
        removeChildren(0,-1,true);
        Starling.juggler.purge();
        var game = new Game(map_name);
        addChild(game);
        Starling.juggler.add(game);
    }
}
