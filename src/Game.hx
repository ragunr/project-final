import bitmasq.Gamepad;
import bitmasq.GamepadEvent;
import starling.display.*;
import starling.animation.*;
import starling.events.*;
import starling.textures.*;
import starling.geom.Polygon;
import flash.geom.Point;
import flash.geom.Rectangle;
import flash.ui.*;
import box2D.dynamics.*;
import box2D.dynamics.contacts.*;
import box2D.common.math.*;
import box2D.collision.*;
import box2D.collision.shapes.*;
import Geom;
import Tilemap;
import DialogueController;
import Light;
import Trigger;
import Platform;
import SoundEffects;
using Geom.Vec2Ext;
using Geom.RectExt;
using Lambda;
using Util;



// Top level game class. This holds the physics enabled world and keeps
// track of all of the major game pieces.
class Game extends AnimSprite {

    public static inline var PIXELS_PER_METER : Float = 72;
    public static inline var PPM : Float = PIXELS_PER_METER;
	
    private static inline var VELOCITY_ITERATIONS : Int = 8;
    private static inline var POSITION_ITERATIONS : Int = 3;

    // Light and dark worlds for compositing
    public var world_lit : Sprite;
    public var world_dark : Sprite;
    public var debugLayer : Sprite;

	public var speech: DialogueController;
	
    // Physics container
    public var world_phys : B2World;
    public var world_phys_listener : GamePhysicsListener;
    public var static_body : B2Body;

    // Entities
    public var entities : Array<Entity>;
    public var entities_by_name : Map<String,Entity>;
    public var player : Player;

    // Lighting
    public var lights : List<Polygon>;
    public var full_light : Bool;

    // Dimensions
    public var pixel_size : Vec2;

    public var camera : Vec2;

    public var ladder: Int = 0;

    public var onFloor: Bool = true;
	
    private var debug : Bool = false;

    public var mapName : String;
    public var events_called : Map<String,Bool>;

    public var sounds: SoundEffects;

    public var gravity: B2Vec2;

    public var use_indicator : Image;

    public function new(map_name:String) {
        super();
        this.mapName = map_name;
        // Set up collections
        entities = new Array<Entity>();
        entities_by_name = new Map<String,Entity>();
        lights = new List<Polygon>();
        full_light = false;

        events_called = new Map<String,Bool>();
		
        // Set up box2d physics world
		gravity = new B2Vec2(0.0, 9.8);
		world_phys = new B2World(gravity, false);
        var static_body_def = new B2BodyDef();
        static_body = world_phys.createBody(static_body_def);
        
        sounds = new SoundEffects();

        //sounds.playEffect(breathing);

        // Set up world spirtes
        world_lit = new Sprite();
        var background = new Quad(10000,1000,0x050515);
        world_lit.addChild(background);
        var lit_tile_layer = new Sprite();
        world_lit.addChild(lit_tile_layer);

        world_dark = new Sprite();
        //var background = new Quad(Root.STAGE_WIDTH,Root.STAGE_HEIGHT,0x000000);
        //world_dark.addChild(background);
        var dark_tile_layer = new Sprite();
        world_dark.addChild(dark_tile_layer);

        debugLayer = new Sprite();

        //This is for the touch control overlay
        var touchOverlay = new TouchOverlay();

        addChild(world_dark);
        addChild(world_lit);
        addChild(debugLayer);
        //This adds the touch control overlay
        addChild(touchOverlay);

		// Load the dialogue
		speech = new DialogueController(Root.assets, "dialogue");
		
        // Load the level
        var tilemap = new Tilemap(Root.assets, map_name,"",registerObject);
        lit_tile_layer.addChild(tilemap);
		lit_tile_layer.flatten();
        var darktilemap = new Tilemap(Root.assets, map_name, "_dark");
        dark_tile_layer.addChild(darktilemap);
		dark_tile_layer.flatten();

        pixel_size = {x:tilemap.mapWidth*tilemap.tileWidth,
            y:tilemap.mapHeight*tilemap.tileHeight};
		
        // Listen for controls
        Gamepad.get().addEventListener(GamepadEvent.CHANGE,gamepadChange);
		
		// Listen for debug
		addEventListener(KeyboardEvent.KEY_DOWN, function(e:KeyboardEvent)
		{

			if (e.keyCode == Keyboard.D) {
				debug = !debug;
				for(entity in entities) {
					entity.deleteCollider();
				}
			}
		});
        
        // Listen for collisions
        world_phys_listener = new GamePhysicsListener(this);
        world_phys.setContactListener(world_phys_listener);

        camera = getCameraFocus();
        //var menu = new Menu(this); //main menu screen
        //menu.addChild(menu); // main menu screen

        MapScripts.fireMapScript(map_name, "0", this);

        use_indicator = new Image(Root.assets.getTexture("useForTouch"));
        use_indicator.x = Root.STAGE_WIDTH/2;
        use_indicator.y = Root.STAGE_HEIGHT/16;
        addChild(use_indicator);
    }

    private function gamepadChange(event:GamepadEvent){
        switch event.control {
            case Gamepad.D_LEFT:
				if (event.value == 0) {
					player.stopMoving();
				} else {
					player.move(Left);
                    player.aimFlashlight(Left);

				}
            case Gamepad.D_RIGHT:
				if (event.value == 0) {
					player.stopMoving();
				} else {
					player.move(Right);
					player.aimFlashlight(Right);

				}
            case Gamepad.D_UP:
                if (ladder > 0)
                {
                    if (event.value == 0) {
                    player.stopMoving();

                    } else {
                        player.move(Up);

                    }
                }
                else {
                    if (event.value == 0) {
                        player.aimFlashlight(null);
                    }
                    else {
                        player.aimFlashlight(Up);
                    }
                }
                // look up
            case Gamepad.D_DOWN:
                if (ladder > 0)
                {
                    if (event.value == 0) {
                    player.stopMoving();

                    }
                    else {
                    player.move(Down);

                    }

                }
                else {
                    if (event.value == 0) {
                        player.aimFlashlight(null);
                    }
                    else {
                        player.aimFlashlight(Down);
                    }
                }
            case Gamepad.A_DOWN:
                player.activate(event.value);
            case Gamepad.A_RIGHT:
                // flashlight button
                if(event.value == 0) player.toggleFlashlight();
            case Gamepad.A_LEFT:
                // Run button
                if(!player.runEnabled) return;
                if(event.value == 1) {
                    player.running = true;
                } else {
                    player.running = false;
                }
                if(player.walking != null)
                {
                    player.stopMoving();
                    player.move(player.faceForward ? Right : Left);
                }

        }
    }

    override public function advanceTime(t:Float){
        super.advanceTime(t);
		
		
        for(entity in entities) {
            entity.advanceTime(t);
        }

        world_phys.step(t, VELOCITY_ITERATIONS, POSITION_ITERATIONS);
		
        applyLighting();
		
        if (debug) {
            for(entity in entities) {
                entity.deleteCollider();
				entity.drawCollider();
            }
        }

        var focus = getCameraFocus().mult(-1).add(
                {x:Root.STAGE_WIDTH/2,y:Root.STAGE_HEIGHT/2});

        var buff = {x:Root.STAGE_WIDTH/3, y:Root.STAGE_HEIGHT/3};
        camera = camera.constrain(focus.sub(buff.div(2)),focus.add(buff.div(2)));
        camera = camera.constrain(
                {x:pixel_size.x-Root.STAGE_WIDTH,y:pixel_size.y-Root.STAGE_HEIGHT}.mult(-1),
                {x:0,y:0}
                );

        camera.apply(world_lit);
        camera.apply(world_dark);
        camera.apply(debugLayer);

        if(player.activateEntity == null){
            use_indicator.visible = false;
        } else {
            use_indicator.visible = true;
        }
    }

    public function getCameraFocus():Vec2{
        return player.getCameraFocus();
    }

    public function registerObject(object:TilemapObject){

        var entity : Entity = null;
        // if we are on the Lighting layer, this is a static light
        if(object.layerName == "Lighting"){
            var poly = object.buildStarlingPolygon();
            entity = new Light(this,poly);
        }
        // Check if we are in the trigger layer
        else if (object.layerName == "Triggers") {
            var shape = object.buildB2Shape(1 / PPM);
			entity = new Trigger(shape, static_body, this);
        }
		else if (object.layerName == "Static") {
			var shape = object.buildB2Shape(1 / PPM);
			entity = new Platform(shape, static_body, this);
		}
        // Check if this is the player spawn
        else if (object.tile != null && object.tile.source == "playerTestLit"){
		    player = new Player(
                    new B2Vec2(
                        (object.x), 
                        (object.y-object.tile.height)), 
                    this);
            entity = player;
		}
		//Check if this is an NPC spawn
		else if (object.tile != null && (object.tile.source == "npcPlaceholder" || object.tile.source == "teenstanding")){
            var animations = {
                stand: (object.customAttr.exists("anim_stand")) ? object.customAttr["anim_stand"] : "teenstanding", 
                walk: (object.customAttr.exists("anim_walk")) ? object.customAttr["anim_walk"] : "Pro_walk"
            }

		    entity = new NPC(
                    new B2Vec2(
                        (object.x), 
                        (object.y-object.tile.height)),
					animations,
                    (object.customAttr.exists("dark_shade") ? Std.parseInt(object.customAttr["dark_shade"]) : 0x222222),
                    this);
		}
		else if (object.tile != null && (object.tile.source == "bellhopPlaceholder" || object.tile.source == "bellhopstanding")){
            var animations = {
                stand: (object.customAttr.exists("anim_stand")) ? object.customAttr["anim_stand"] : "bellhopstanding", 
                walk: (object.customAttr.exists("anim_walk")) ? object.customAttr["anim_walk"] : "Pro_walk",
				talk: (object.customAttr.exists("anim_talk")) ? object.customAttr["anim_talk"] : "bellhoptalking"
            }
		    entity = new NPC(
                    new B2Vec2(
                        (object.x), 
                        (object.y-object.tile.height)), 
					animations,
                    (object.customAttr.exists("dark_shade") ? Std.parseInt(object.customAttr["dark_shade"]) : 0x222222),
                    this);
		}
		else if (object.tile != null && (object.tile.source == "businessmanPlaceholder" || object.tile.source == "buisness woman standing copy")) {
            var animations = {
                stand: (object.customAttr.exists("anim_stand")) ? object.customAttr["anim_stand"] : "buisness woman standing copy", 
                walk: (object.customAttr.exists("anim_walk")) ? object.customAttr["anim_walk"] : "Pro_walk",
				talk: (object.customAttr.exists("anim_talk")) ? object.customAttr["anim_talk"] : "buisnesstalking"
            }
		    entity = new NPC(
                    new B2Vec2(
                        (object.x), 
                        (object.y-object.tile.height)), 
					animations,
                    (object.customAttr.exists("dark_shade") ? Std.parseInt(object.customAttr["dark_shade"]) : 0x222222),
                    this);
		}
		
        // Check if this is a door;
        else if (object.tile != null && object.tile.source == "door1"){
            var position = new B2Vec2(object.x,object.y-object.tile.height);
            var body_def = new B2BodyDef();
            var fixture_def = new B2FixtureDef();
            body_def.type = KINEMATIC_BODY;
            fixture_def.density = 0;
            fixture_def.friction = (object.customAttr.exists("friction")) ? 
                Std.parseFloat(object.customAttr["friction"]) : 1; 
            fixture_def.restitution = (object.customAttr.exists("restitution")) ? 
                Std.parseFloat(object.customAttr["restitution"]) : 0; 
	    fixture_def.shape = object.buildB2Shape(1, true );
            var lit_sprite = new AnimSprite();
            lit_sprite.addChild(new Image(Root.assets.getTexture(object.tile.source)));
            var dark_sprite = new AnimSprite();
            dark_sprite.addChild(new Image(Root.assets.getTexture(object.tile.source+"_dark")));
            var pentity = new PhysicsEntity(position,lit_sprite,dark_sprite,body_def, this);
			pentity.setCategory(CollisionFilters.Foreground);
			var mask = CollisionFilters.Platform | CollisionFilters.Player | CollisionFilters.Foreground;
			pentity.setMask(mask);
			pentity.createFixture(fixture_def);
			entity = pentity;
		}
        else if (object.tile != null && object.customAttr.exists("density")){
            var position = new B2Vec2(object.x,object.y-object.tile.height);
            var body_def = new B2BodyDef();
            var fixture_def = new B2FixtureDef();
            if(object.customAttr["density"] == "0"){
                body_def.type = KINEMATIC_BODY;
                fixture_def.density = 0;
            } else {
                body_def.type = DYNAMIC_BODY;
                fixture_def.density = Std.parseFloat(object.customAttr["density"]);
            }
            fixture_def.friction = (object.customAttr.exists("friction")) ? 
                Std.parseFloat(object.customAttr["friction"]) : 1; 
            fixture_def.restitution = (object.customAttr.exists("restitution")) ? 
                Std.parseFloat(object.customAttr["restitution"]) : 0; 
	    fixture_def.shape = object.buildB2Shape(1, true );
            var lit_sprite = new AnimSprite();
            lit_sprite.addChild(new Image(Root.assets.getTexture(object.tile.source)));
            var dark_sprite = new AnimSprite();
            dark_sprite.addChild(new Image(Root.assets.getTexture(object.tile.source+"_dark")));
            var pentity = new PhysicsEntity(position,lit_sprite,dark_sprite,body_def, this);
			if (object.customAttr["collision_filter"] == "foreground") {
				pentity.setCategory(CollisionFilters.Foreground);
				var mask = CollisionFilters.Platform | CollisionFilters.Player | CollisionFilters.Foreground;
				pentity.setMask(mask);
			} else if (object.customAttr["collision_filter"] == "background") {
				pentity.setCategory(CollisionFilters.Background);
				var mask = CollisionFilters.Platform | CollisionFilters.NPC | CollisionFilters.Background;
				pentity.setMask(mask);
			}
			pentity.createFixture(fixture_def);
			entity = pentity;
        }
        else { return; }

        if(object.customAttr.exists("script"))
        {
            var params = new Array<String>();
            var i = 1;
            while(true){
                if(!object.customAttr.exists("script_param_"+i)) break;
                params.push(object.customAttr["script_param_"+i]);
                i += 1;
            }
            entity.fireScript(object.customAttr["script"],params);
        }
        entity.name = object.name;
		entities.push(entity);
        if (entity.name != null) entities_by_name[entity.name] = entity;
    }

    override function dispose() {
        for(entity in entities){
            entity.dispose();
        }
    }

    private function applyLighting(){
        if(world_lit.mask != null) world_lit.mask.dispose();
        if(full_light) {
            world_lit.mask = null;
            return;
        }
        var stage_poly = new Rectangle(-world_lit.x,-world_lit.y,Root.STAGE_WIDTH,Root.STAGE_HEIGHT);
        //trace(stage_poly);
        var light_mask = new Canvas();
        for(light in lights){
            //trace(Util.getPolygonBounds(light));
            if(!stage_poly.intersects(Util.getPolygonBounds(light))) continue;
            light_mask.drawPolygon(light);
        }
        world_lit.mask = light_mask;
        
    }
	
	// this method is for playing events. It's called in Trigger currently
	// and takes sc which is the name of a trigger. In Tiled you can see 
	// give a Trigger object a custom attribute script_param_1 which equals
	// thet trigger name in the dialogue XML.
	public function playScene(sc:String,?onComplete:Void->Void) {
		// stop the player moving so he doesn't slide along
		player.stopMoving();
		var convo = speech.playDialogue(sc); // returns all dialogues in this conversation
		var dialogues = convo.dialogues;
		var i = 0;
		if (dialogues[0].text_queue.length == 0) return; // don't display an empty dialogue
		
		var dia = new Dialogue(dialogues[0].title, dialogues[0].text_queue.list(), convo.autoPlay);
		var person = null;
		if (dia.title == 'Elizabeth') {
			person = cast(entities_by_name["business"], NPC);
			person.talk();
		}
		else if (dia.title == 'Natsuki') {
			person = cast(entities_by_name["bellhop"], NPC);
			person.talk();
		}
		addChild(dia);
		juggler.add(dia);
		dia.onComplete = function() {
			if (person != null) person.stopTalking();
			juggler.remove(dia);
			dia.removeFromParent(true);
			i++;
			if ( dialogues.length > i )	{
				var exitfunction = dia.onComplete;
				dia = new Dialogue(dialogues[i].title, dialogues[i].text_queue.list(), convo.autoPlay);
				
				if (dia.title == 'Elizabeth') {
					person = cast(entities_by_name["business"], NPC);
					person.talk();
				}
				else if (dia.title == 'Natsuki') {
					person = cast(entities_by_name["bellhop"], NPC);
					person.talk();
				}
				addChild(dia);
				juggler.add(dia);
				dia.onComplete = exitfunction;
			}
            else if(onComplete != null) {
                onComplete();
            }
		
		};
		
	}

}

class GamePhysicsListener extends B2ContactListener {
    var game: Game;
    public function new(game:Game){
        super();
        this.game = game;
    }

    override function beginContact(contact:B2Contact){
        if(!contact.isTouching()) return;
        if(cast(contact.getFixtureA().getUserData(),Entity).beginContactCallback != null){
            cast(contact.getFixtureA().getUserData(),Entity).beginContactCallback(contact);
        }
        if(cast(contact.getFixtureB().getUserData(),Entity).beginContactCallback != null){
            cast(contact.getFixtureB().getUserData(),Entity).beginContactCallback(contact);
        }
    }

    override function endContact(contact:B2Contact){
        if(cast(contact.getFixtureA().getUserData(),Entity).endContactCallback != null){
            cast(contact.getFixtureA().getUserData(),Entity).endContactCallback(contact);
        }
        if(cast(contact.getFixtureB().getUserData(),Entity).endContactCallback != null){
            cast(contact.getFixtureB().getUserData(),Entity).endContactCallback(contact);
        }
    }

    override function preSolve(contact:B2Contact, manifold:B2Manifold){
        if(cast(contact.getFixtureA().getUserData(),Entity).preSolveCallback != null){
            cast(contact.getFixtureA().getUserData(),Entity).preSolveCallback(contact,manifold);
        }
        if(cast(contact.getFixtureB().getUserData(),Entity).preSolveCallback != null){
            cast(contact.getFixtureB().getUserData(),Entity).preSolveCallback(contact,manifold);
        }
    }

    override function postSolve(contact:B2Contact,impulse:B2ContactImpulse){
        if(cast(contact.getFixtureA().getUserData(),Entity).postSolveCallback != null){
            cast(contact.getFixtureA().getUserData(),Entity).postSolveCallback(contact,impulse);
        }
        if(cast(contact.getFixtureB().getUserData(),Entity).postSolveCallback != null){
            cast(contact.getFixtureB().getUserData(),Entity).postSolveCallback(contact,impulse);
        }
    }
}

// Entity class represents a single object in the game and is responsible
// for creating and acting as a controller between box2d bodies and spriets
// Subclass this for each type of thing
class Entity {
    public var game : Game;
    public var juggler : Juggler;

    public var beginContactCallback : B2Contact -> Void = null;
    public var endContactCallback : B2Contact -> Void = null;
    public var preSolveCallback : B2Contact -> B2Manifold -> Void = null;
    public var postSolveCallback : B2Contact -> B2ContactImpulse -> Void = null;
    public var beginActivateCallback : Void -> Void = null;
    public var endActivateCallback : Void -> Void = null;

    public var name: String;

    // override this to instantiate sprites, b2bodies/b2fixtures and lights
    public function new(game:Game){
        this.game = game;
        juggler = new Juggler();
    }

    // override to keep the elements syncronized, and handle game state here
    public function advanceTime(t:Float){
        juggler.advanceTime(t);
    }
	
    // override to call dispose on all of elements so we don't get memeory leaks
	public function dispose() {
	}
	
	// override to draw the collider
	public function drawCollider() {
	}
	
	// override to delete the collider drawing
	public function deleteCollider() {
	}

    public function fireScript(script_name:String,parameters:Array<String>) {
        throw "Script not found: "+script_name;
    }
}

// Its a sprite with a built in juggler. Automaticly tracks its lifetime,
// Fill it with movie clips on its own juggler, and then add the entire
// sprite to a juggler.
class AnimSprite extends Sprite implements IAnimatable {

    public var juggler : Juggler;
    public var elapsedTime(get, null) : Float;
    public var animations : Map<String,Array<AnimationInfo>>;
    public var current_animation : String;
    public var current_animation_scale : Vec2;

    public function new(){
        super();
        juggler = new Juggler();
        animations = new Map<String,Array<AnimationInfo>>();
        current_animation = "";
        current_animation_scale = {x:1,y:1};
    }

    public function advanceTime(t) {
        juggler.advanceTime(t);
    }

    function get_elapsedTime() {
        return juggler.elapsedTime;
    }

    public function addAnimation(animation_name : String, movie_clip : MovieClip){
        if(!animations.exists(animation_name)) 
            animations.set(animation_name, new Array<AnimationInfo>());
        var scale = {x: movie_clip.scaleX, y:movie_clip.scaleY};
        animations.get(animation_name).push({movieClip: movie_clip,scale: scale});
        movie_clip.visible = false;
        movie_clip.stop();
        juggler.add(movie_clip);
    }

    public function playAnimation(animation_name : String, ?scale : Vec2, ?pause : Bool) {
        if(scale == null) scale = {x:1,y:1};
        if(pause == null) pause = false;
        if(animation_name != current_animation ||
                !scale.equals(current_animation_scale)){
            if(current_animation != "") {
                for(animinfo in animations.get(current_animation)){
                    var anim = animinfo.movieClip;
                    anim.stop();
                    anim.visible = false;
                }
            }

            if(animation_name != "") {
                for(animinfo in animations.get(animation_name)){
                    var anim = animinfo.movieClip;
                    if(!pause) anim.play();
                    else anim.pause();
                    anim.visible = true;
                    anim.scaleX = animinfo.scale.x*scale.x;
                    anim.scaleY = animinfo.scale.y*scale.y;
                }
            }
        }
        else if(current_animation != "") {
                for(animinfo in animations.get(current_animation)){
                    var anim = animinfo.movieClip;
                    if(!pause) anim.play();
                    else anim.pause();
                }
        }
        current_animation = animation_name;
        current_animation_scale = scale;
    }

    public function pauseAnimation()
    {
        if(current_animation != "") {
            for(animinfo in animations.get(current_animation)){
                var anim = animinfo.movieClip;
                anim.pause();
            }
        }
    }

}

typedef AnimationInfo = {
    var movieClip : MovieClip;
    var scale : Vec2;
}

enum Direction {
	Left;
	Right;
	Up;
	Down;
}

class CollisionFilters {
	public static inline var Platform =   0x0001;
	public static inline var Player =     0x0002;
	public static inline var NPC =        0x0004;
	public static inline var Foreground = 0x0008;
	public static inline var Background = 0x0010;
}
