import box2D.dynamics.*;
import box2D.dynamics.contacts.B2Contact;
import box2D.common.math.*;
import box2D.collision.shapes.*;
import starling.display.Quad;
import Game;
import Geom;
import Tilemap;
import MapScripts;
import Type.*;
using Geom.Vec2Ext;
using Geom.RectExt;
using Lambda;

class Trigger extends FixturePhysicsEntity {

	public function new(shape:B2Shape, body:B2Body, game:Game){
		var fixtureDef = new B2FixtureDef();
		fixtureDef.shape = shape;
		fixtureDef.isSensor = true;
        super(fixtureDef, body, game);
		
		debugColor = 0x0000ff;
    }
	public override function drawCollider() {
		if (fixture != null) {
			var shapeType = fixture.getType();
			if (shapeType == POLYGON_SHAPE) {
				var polygonShape = cast(fixture.getShape(), B2PolygonShape);
				var vertices = polygonShape.getVertices();
				if (vertices.length > 2) {
					for (i in 0...vertices.length - 1) {
						drawColliderSegment(vertices[i], vertices[i+1], i);
					}
					drawColliderSegment(vertices[vertices.length - 1], vertices[0], vertices.length - 1);
				}
			}
		}
	}
	
	override function drawColliderSegment(vert1:B2Vec2, vert2:B2Vec2, i:Int) {
		var pos1 = new B2Vec2((game.static_body.getPosition().x + vert1.x), (game.static_body.getPosition().y + vert1.y));
		var pos2 = new B2Vec2((game.static_body.getPosition().x + vert2.x), (game.static_body.getPosition().y + vert2.y));
		if (game.static_body.getAngle() != 0) {
			pos1 = Util.rotateAroundPoint(pos1, game.static_body.getPosition(), game.static_body.getAngle());
			pos2 = Util.rotateAroundPoint(pos2, game.static_body.getPosition(), game.static_body.getAngle());
		}
		pos1.x *= Game.PPM;
		pos1.y *= Game.PPM;
		pos2.x *= Game.PPM;
		pos2.y *= Game.PPM;
		var quad = Util.createLine(pos1, pos2, 0x0000FF);
		colliderLines[i] = quad;
		game.debugLayer.addChild(quad);
	}
	
	public override function deleteCollider() {
		for (i in 0...colliderLines.length) {
			game.debugLayer.removeChild(colliderLines[i]);
		}
	}

    override function fireScript(script_name:String, params:Array<String>) {
        switch(script_name){
            case "placeholder": scriptTraceEnterExit(params[0],params[1]);
            case "fireMapEvent": scriptFireMapEvent(params[0]);
            case "fireActivatedMapEvent": scriptFireActivatedMapEvent(params[0]);
			case "exitFireMapEvent": exitScriptFireMapEvent(params[0]); // for exiting a trigger area
            case "teleport":
                var params0 = params[0].split(",");
                scriptTeleport({x:Std.parseFloat(params0[0]),
                        y:Std.parseFloat(params0[1])});
            case "playScene": scriptPlayScene(params[0]);
            case "nextLevel": scriptNextLevel(params[0]);
            case "ladder": scriptLadder();
            default: super.fireScript(script_name,params);
        }
    }


    public function scriptTraceEnterExit(enter_msg:String,exit_msg:String){
        beginContactCallback = function(contact:B2Contact){
			trace(enter_msg);
        }
        endContactCallback = function(contact:B2Contact){
            trace(exit_msg);
        }
    }

	public function exitScriptFireMapEvent(event_key) {
		endContactCallback = function(contact:B2Contact){
		if(game.events_called.exists(event_key)) {
			//trace("no");
			return;
		}
		game.events_called[event_key] = true;
		//trace("go");
		MapScripts.fireMapScript(game.mapName,event_key, game);
	}
	}
	
    public function scriptFireMapEvent(event_key)
    {
        beginContactCallback = function(contact:B2Contact){
        if(contact.getFixtureA().getUserData() != game.player &&
            contact.getFixtureB().getUserData() != game.player) return;
        if(game.events_called.exists(event_key)) {
			//trace("no");
			return;
		}
		game.events_called[event_key] = true;
		//trace("go");
		MapScripts.fireMapScript(game.mapName,event_key, game);
        }
    }
	
    public function scriptFireActivatedMapEvent(event_key)
    {
        beginContactCallback = function(contact:B2Contact){
            game.player.activateEntity = this;
        }
        endContactCallback = function(contact:B2Contact){
            if(game.player.activateEntity == this) 
                game.player.activateEntity = null;
        }
        beginActivateCallback = function(){
        if(game.events_called.exists(event_key)) {
			trace("no");
			return;
		}
		game.events_called[event_key] = true;
		MapScripts.fireMapScript(game.mapName,event_key, game);
        }
    }

    public function scriptTeleport(offset : Vec2){
        beginContactCallback = function(contact:B2Contact){
            game.player.activateEntity = this;
        }
        endContactCallback = function(contact:B2Contact){
            if(game.player.activateEntity == this) 
                game.player.activateEntity = null;
        }
        beginActivateCallback = function(){
            game.player.teleport(offset);
        }
    }

    public function scriptPlayScene(scene_name) {
        beginContactCallback = function(contact:B2Contact){
            game.playScene(scene_name);
            beginContactCallback = null;
            beginContactCallback = null;
        }
    }

    public function scriptNextLevel(map_name:String){
        beginContactCallback = function(contact:B2Contact){
            Root.get().nextLevel(map_name);
        }
    }

    public function scriptLadder()
    {

        beginContactCallback = function(contact:B2Contact){

            var playerPos = game.player.body.getPosition();
            playerPos.y += 48;

            //game.world_phys.queryPoint( onFloor,  playerPos);

            if(contact.getFixtureA().getUserData() != game.player &&
                contact.getFixtureB().getUserData() != game.player) return;
            game.ladder += 1;
        }

         endContactCallback = function(contact:B2Contact){
            if(contact.getFixtureA().getUserData() != game.player &&
                contact.getFixtureB().getUserData() != game.player) return;
            game.ladder -= 1;
            if(game.ladder == 0) game.player.stopMoving();
        }
    }

    // private function onFloor(fixture:B2Fixture) {                                                                                                               
    //     var body:B2Body = fixture.getBody();           
    //      game.onFloor = true;
    //      return false;

    //     return true;
    // }
}

