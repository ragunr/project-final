import bitmasq.Gamepad;
import bitmasq.GamepadEvent;
import starling.display.*;
import starling.utils.*;
import starling.core.Starling;
import starling.animation.Transitions;
import starling.events.Event;
import starling.events.KeyboardEvent;
import starling.events.*;
import flash.media.SoundChannel;

class Menu extends Sprite
{
    public var menu:Image;
    public var menuButton:Image;
    public var player:MovieClip;

    public function new()
    {
            super();
            menu = new Image(Root.assets.getTexture("titlescreen"));
            addChild(menu);
            menuButton = new Image(Root.assets.getTexture("titlebuttons"));
            menuButton.addEventListener("touch", menuFunc);
            //menuButton.x = (Root.STAGE_WIDTH/2)-menuButton.width;
            //menuButton.y = (Root.STAGE_HEIGHT/2)-menuButton.height;
            addChild(menuButton);
            player = new MovieClip(Root.assets.getTextures("Pro_walk"));
            player.x = 750;
            player.y = 875;
            player.scaleX = player.scaleY = 0.6;
            addChild(player);
            var tree = new Quad(100,100, 0x766454);
            tree.x = 714;
            tree.y = 850;
            addChild(tree);
            Starling.juggler.add(player);
            Gamepad.get().addEventListener(GamepadEvent.CHANGE, gamepadChange);
            addEventListener(KeyboardEvent.KEY_DOWN, keydown);
    }

    private function keydown(e:KeyboardEvent){
        if("123456789".indexOf(String.fromCharCode(e.charCode))>=0) {
            Root.get().startGame("map_"+String.fromCharCode(e.charCode));
        }
    }
    

    private function gamepadChange(event:GamepadEvent){
        switch event.control {
            case Gamepad.D_LEFT:
				if (event.value == 0) {
				} else {
				}
            case Gamepad.D_RIGHT:
				if (event.value == 0) {
				} else {
				}
            case Gamepad.D_UP:
                // look up
            case Gamepad.D_DOWN:
                // look down
            case Gamepad.A_DOWN:
                //Chearp replacement for gamepad funcitonality
                //menuFunc(null);
                Starling.juggler.tween(menuButton,1.5,{
                    transition: Transitions.EASE_IN_OUT,
                    alpha:0
                    });
                Starling.juggler.tween(player,3.0,{
                    x:player.x+150
                    });
                Starling.juggler.delayCall(Root.get().startGame, 3.0);
                //use button
            case Gamepad.A_RIGHT:
                // flashlight button
            case Gamepad.A_LEFT:
                // Run button
        }
    }

    public function menuFunc(e:TouchEvent)
    {
        /*Gamepad.get().addEventListener(GamepadEvent.CHANGE, gamepadChange);
        addEventListener(KeyboardEvent.KEY_DOWN, function(e:KeyboardEvent)
        {
            if (e.keyCode == Keyboard.MENU)
            {
                debug = !debug;
                Sprite.removeChildren();
                var game = new Game(Sprite);
                game.start();
            }
        });*/
        var touch:Touch = e.getTouch(this);
        if (touch!= null)
        {
            if (touch.phase == TouchPhase.BEGAN)
            {
                //Root.assets.removeSound("startmusic");
                Root.get().startGame();
            }
        }
    }
}

//For keyboard events just create keyboard events and dispatch them to root
