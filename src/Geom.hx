import starling.display.DisplayObject;
import Math;
using Geom.Vec2Ext;
using Geom.RectExt;

class Geom{
}

typedef Vec2 = { 
    var x : Float; 
    var y : Float;
}

class Vec2Ext {
    public static inline function equals(p1:Vec2,p2:Vec2):Bool{
        return p1.x == p2.x && p1.y == p2.y;
    }
    public static inline function add(p1:Vec2, p2:Vec2):Vec2{
        return {x:p1.x+p2.x, y:p1.y+p2.y};
    }

    public static inline function sub(p1:Vec2, p2:Vec2):Vec2{
        return {x:p1.x-p2.x, y:p1.y-p2.y};
    }

    public static inline function mult(p1:Vec2, s:Float):Vec2{
        return {x:p1.x*s, y:p1.y*s};
    }

    public static inline function div(p1:Vec2, s:Float):Vec2{
        return {x:p1.x/s, y:p1.y/s};
    }

    public static inline function rotate(p1:Vec2, rad:Float):Vec2{
        var costheta = Math.cos(rad);
        var sintheta = Math.sin(rad);
        return {x:p1.x*costheta-p1.y*sintheta,
                y:p1.x*sintheta+p1.y*costheta};

    }

    public static inline function dot(p1:Vec2, p2: Vec2):Float{
        return p1.x * p2.x + p1.y * p2.y;
    }

    public static inline function mag(p1:Vec2):Float{
        return Math.sqrt(p1.x*p1.x + p1.y*p1.y);
    }

    public static function normalize(p1:Vec2,?mag:Float):Vec2{
        if(p1.mag() == 0) return p1;
        if(mag == null) mag = 1;
        return p1.div(p1.mag()).mult(mag);
    }

    public static inline function project(p1:Vec2, p2:Vec2):Vec2{
        var p2norm = p2.normalize();
        return p2norm.mult(p1.dot(p2.normalize()));
    }

    public static inline function project_perp(p1: Vec2, p2:Vec2):Vec2{
        return p1.sub(project(p1,p2));
    }

    public static function apply(p1:Vec2, target:Dynamic){
        target.x = p1.x;
        target.y = p1.y;
    }

    public static function apply_offset(p1:Vec2, target:Dynamic){
        target.x += p1.x;
        target.y += p1.y;
    }

    public static inline function getVec2(target:DisplayObject):Vec2{
        return{x: target.x, y: target.y}
    }

    public static inline function limit_mag(p1:Vec2, limit:Float){
        return (p1.mag() <= limit) ? p1 : p1.div(p1.mag()/limit);
    }

    public static inline function constrain(p1:Vec2, pmin:Vec2, pmax:Vec2) {
        return {
            x: Math.min(Math.max(p1.x, pmin.x),pmax.x), 
            y: Math.min(Math.max(p1.y, pmin.y),pmax.y)
        }
    }
}

typedef Rect = { > Vec2,
    var w : Float;
    var h : Float;
}

class RectExt{
    public static inline function offset(r1:Rect, p1:Vec2):Rect{
        return {x: r1.x + p1.x, y: r1.y + p1.y, w: r1.w, h: r1.h};
    }

    public static inline function area(r1:Rect):Float{
        return r1.w * r1.h;
    }

    public static inline function x1(r1:Rect):Float{
        return r1.x;
    }

    public static inline function x2(r1:Rect):Float{
        return r1.x+r1.w;
    }

    public static inline function y1(r1:Rect):Float{
        return r1.y;
    }

    public static inline function y2(r1:Rect):Float{
        return r1.y+r1.h;
    }

    public static inline function topLeft(r1:Rect):Vec2{
        return {x: r1.x1(), y: r1.y1()};
    }

    public static inline function topRight(r1:Rect):Vec2{
        return {x: r1.x2(), y: r1.y1()};
    }

    public static inline function bottomLeft(r1:Rect):Vec2{
        return {x: r1.x1(), y: r1.y1()};
    }

    public static inline function bottomRight(r1:Rect):Vec2{
        return {x: r1.x2(), y: r1.y2()};
    }

    public static inline function center(r1:Rect):Vec2{
        return {x: r1.x + r1.w*0.5, y: r1.y + r1.h*0.5};
    }

    public static inline function contains(r1:Rect, p1:Vec2){
        return p1.x > r1.x1() && 
            p1.x < r1.x2() &&
            p1.y > r1.y1() && 
            p1.y < r1.y2();
    }

    public static inline function intersection(r1:Rect, r2:Rect):Rect{
        var x1 = Math.max(r1.x1(), r2.x1());
        var y1 = Math.max(r1.y1(), r2.y1());
        var x2 = Math.min(r1.x2(), r2.x2());
        var y2 = Math.min(r1.y2(), r2.y2());
        return {x: x1, y: y1, w: Math.max(0,x2-x1), h: Math.max(0,y2-y1)}
        
    }

    public static inline function intersects(r1:Rect, r2:Rect):Bool{
        return area(intersection(r1,r2)) > 0;
    }

    public static function collisionNormal(r1:Rect, r2:Rect):Vec2{
        var inter = r1.intersection(r2);
        if(inter.w < inter.h) {
            if(r1.center().x <= r2.center().x)
                return {x:inter.w, y: 0};
            else 
                return {x:-inter.w,y: 0};
        } else {
            if(r1.center().y <= r2.center().y)
                return {x: 0, y: inter.h};
            else 
                return {x: 0, y: -inter.h};
        }
        /*
        var x1_pen = r2.x2() - r1.x1();
        var x2_pen = r1.x1() - r1.x2();
        var y1_pen = r2.y2() - r1.y1();
        var y2_pen = r1.y1() - r1.y2();
        var least_pen_normal:Vec2 = {x:0.0, y:0.0}
        var least_pen_mag = Math.POSITIVE_INFINITY;
        if(x1_pen > 0 && x1_pen < least_pen_mag){
            least_pen_mag = x1_pen;
            least_pen_normal = {x:-x1_pen, y:0};
        }
        if(x2_pen > 0 && x2_pen < least_pen_mag){
            least_pen_mag = x2_pen;
            least_pen_normal = {x:x2_pen, y:0};
        }
        if(y1_pen > 0 && y1_pen < least_pen_mag){
            least_pen_mag = y1_pen;
            least_pen_normal = {x:0, y:-y1_pen};
        }
        if(y2_pen > 0 && y2_pen < least_pen_mag){
            least_pen_mag = y2_pen;
            least_pen_normal = {x:0, y:y2_pen};
        }
        trace(least_pen_normal);
        return least_pen_normal;
        */
    }
}
