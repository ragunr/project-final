import starling.display.*;
import starling.animation.*;
import starling.events.*;
import starling.textures.*;
import starling.geom.Polygon;
import box2D.dynamics.*;
import box2D.common.math.*;
import Game;
import Geom;
import Tilemap;
using Geom.Vec2Ext;
using Geom.RectExt;
using Lambda;
import Random;

class Light extends Entity{
    public var light_on = true;
    public var polygon : Polygon;

    public function new(game:Game,polygon:Polygon){
        super(game);
        polygon = polygon.clone();
        this.polygon = polygon;
        game.lights.add(polygon);
    }

    public function switch_on(){
        if(!light_on){
            light_on = true;
            game.lights.add(polygon);
        }
    }

    public function switch_off(){
        if(light_on){
            light_on = false;
            game.lights.remove(polygon);
        }
    }

    override function dispose(){
        switch_off();
        game.lights.remove(polygon);
    }

    override function fireScript(script_name:String,params:Array<String>){
        if(script_name == "LightFlicker") {
            scriptLightFlicker(Std.parseInt(params[0]));
        }
        else{
            super.fireScript(script_name,params);
        }
    }

    private static inline var FLICKER_SPEED = 0.4;
    public function scriptLightFlicker(seed:Int){
        var loop = new DelayedCall(null,FLICKER_SPEED);
        var rand = new Random(seed);
        var flicker = function(dc:DelayedCall) {
                var delay1 = rand.float()/2*FLICKER_SPEED;
                var delay2 = rand.float()/2*FLICKER_SPEED;
                var delay0 = (1.0 - delay1 - delay2)*FLICKER_SPEED;
                juggler.delayCall(switch_off,delay0+delay1);
                juggler.delayCall(switch_on,FLICKER_SPEED);
                juggler.advanceTime(delay0);
            } 
        loop.reset(flicker,FLICKER_SPEED,[loop]);
        loop.repeatCount = 0;
        loop.advanceTime(1.0);
        juggler.add(loop);
    }

}

class LightBeam extends Light {
    public static inline var BEAM_RAYS = 32;

    public var position:Vec2;
    public var direction:Vec2;
    public var length : Float;
    public var spread : Float;

    public function new(game:Game,direction:Vec2,?length:Float,?spread:Float) {
        var initial_points = new Array();
        for(i in 0...BEAM_RAYS+2) {
            initial_points.push(0);
            initial_points.push(0);
        }
        super(game, new Polygon(initial_points));
        this.direction = direction;
        this.position = {x:0,y:0};
        this.length = (length != null) ? length : 1300;
        this.spread = (spread != null) ? spread : Math.PI*3/16;
    }

    override function advanceTime(t){
        var pos = position;
        for(i in 0...BEAM_RAYS) {
            var start = pos;
            var delta = direction.mult(length).rotate(-spread/2 + spread/(BEAM_RAYS-1)*i);
            start = start.add(delta.normalize(8));
            var end = pos.add(delta);
            var start_index = (BEAM_RAYS*2-i)%(BEAM_RAYS*2);
            var end_index = i+1;

            //var prev_ratio = 1;

            delta = delta.normalize((Math.sin(Math.PI/4+Math.PI/2*i/BEAM_RAYS)-Math.sin(Math.PI/4))*Game.PPM*2);
            delta = delta.rotate((-spread/2 + spread/(BEAM_RAYS-1)*i)*3);

            //Shorten rays based on Box2d World
            function rayCastCallback(fixture:B2Fixture,point:B2Vec2,normal:B2Vec2,fraction:Float):Dynamic{
                if(fixture.isSensor()) return 1;
                if(fixture.getFilterData().categoryBits != CollisionFilters.Platform) return 1;
                //if(fraction >= prev_ratio) return fraction;
                end = {x:point.x*Game.PPM+delta.x,y:point.y*Game.PPM+delta.y};
                return fraction;
            }
            game.world_phys.rayCast(
                    rayCastCallback,
                    new B2Vec2(start.x/Game.PPM,start.y/Game.PPM),
                    new B2Vec2(end.x/Game.PPM,end.y/Game.PPM));

            if(i == 0 || i == BEAM_RAYS-1)polygon.setVertex(start_index,start.x,start.y);
            polygon.setVertex(end_index,end.x,end.y);
        }
    }
}
