import flash.media.Sound;
import flash.media.SoundChannel;
import flash.media.SoundTransform;
import flash.events.*;
import Root;


enum Effect
{
	bite1;
	breathing;
	growl1;
	growl2;
	growl3;
	hit1;
	
}
enum Song{
	GameMusic1;
}

class SoundEffects{

	// Sound variables
	public var musicChannel:SoundChannel;
	public var walkChannel:SoundChannel;
	public var effectChannel:SoundChannel;

	var transform:SoundTransform;

	var pausePos:Float;

	public var walk: Sound;

	public function new(){

		walk = Root.assets.getSound("walking1");
		//walkChannel = walk.play();

		//walkChannel.addEventListener(Event.SOUND_COMPLETE, walkHandler);
		transform = new SoundTransform(0);

	}

	public function playMusic(toPlay: Song){
		musicChannel = Root.assets.playSound("" + toPlay,0,1);
	}

	public function playEffect(toPlay: Effect){
		effectChannel = Root.assets.playSound("" + toPlay,0,1);
	}

	public function playWalk(){
		transform = walkChannel.soundTransform;
		transform.volume = 1;
		walkChannel.soundTransform = transform;


		
	}

	public function walkHandler(event: Event){
		walkChannel = walk.play();
		walkChannel.addEventListener(Event.SOUND_COMPLETE, walkHandler);
	}

	public function stopWalk(){
		transform = walkChannel.soundTransform;
		transform.volume = 0;
		walkChannel.soundTransform = transform;
		

	}

}
