##
## This section has common project settings you may need to change.
##

ifeq ($(OS),Windows_NT)
   FLEX_SDK?=c:/FlexSDK
   ANDROID_SDK=c:/android-sdk
else
   FLEX_SDK=~/ApacheFlex
   ANDROID_SDK=~/android-sdks
endif

#Change APP=game to APP=ld28 to test on android device?
APP=game
FLEX_SDK ?= C:/flexsdk
ADL ?= $(FLEX_SDK)/bin/adl.exe

#Change from $(APP) to ld28 to test on android?
APP_XML=ld28.xml

#Part of the line below is commented out because I don't have those files
#SOURCES=src/Startup.hx src/Root.hx src/Dialogue.hx src/DialogueController.hx src/FixturePhysicsEntity.hx src/Game.hx src/Geom.hx src/Light.hx src/MapScripts.hx src/Menu.hx src/NPC.hx src/PhysicsEntity.hx src/Platform.hx src/Player.hx src/Random.hx src/SoundEffects.hx src/SpriteEntity.hx src/Tilemap.hx src/TouchOverlay.hx src/Trigger.hx src/Util.hx src/assets.xml

##
## It's less common that you would need to change anything after this line.
##

SIGN_CERT=sign.pfx

SIGN_PWD=abc123

SWF_VERSION=11.8

ADL=$(FLEX_SDK)/bin/adl

ADT=$(FLEX_SDK)/bin/adt

AMXMLC=$(FLEX_SDK)/bin/amxmlc
AMXMLC=$(FLEX_SDK)/bin/amxmlc

SOURCES=src/*.hx assets/maps/*.tmx src/*.xml

##
## Build rules
##

all: $(APP).swf $(APP)Web.swf

#Uncomment this to make your .apk file
apk: $(APP).apk

$(APP).swf: $(SOURCES)
	haxe \
		-cp src \
		-cp vendor \
		-lib air3 \
		-lib box2d \
		-swf-version $(SWF_VERSION) \
		-swf-header 1920:1080:60:000000 \
		-main Startup \
		-swf-lib vendor/starling.swc --macro "patchTypes('vendor/starling.patch')" \
		-swf-lib vendor/gamepad.swc \
		-resource assets/maps/test_map.tmx@test_map \
		-resource assets/maps/map_1.tmx@map_1 \
		-resource assets/maps/map_2.tmx@map_2 \
		-resource assets/maps/map_3.tmx@map_3 \
		-resource assets/maps/map_4.tmx@map_4 \
		-resource assets/maps/map_5.tmx@map_5 \
		-resource assets/maps/map_7.tmx@map_7 \
		-resource assets/maps/map_8.tmx@map_8 \
		-resource assets/maps/map_9.tmx@map_9 \
		-resource assets/maps/PuzzleMap1.tmx@PuzzleMap1 \
		-resource assets/dialogue.xml@dialogue \
		-swf $(APP).swf 

clean:
	rm -rf $(APP).swf $(APP).apk

test: $(APP).swf
	$(ADL) -profile tv -screensize 1280x720:1280x720 $(APP_XML)

testLow: $(APP).swf
	$(ADL) -profile tv -screensize 1024x576:1024x576 $(APP_XML)

testHi: $(APP).swf
	$(ADL) -profile tv -screensize 1920x1080:1920x1080 $(APP_XML)

sign.pfx:
	$(ADT) -certificate -validityPeriod 25 -cn SelfSigned 1024-RSA $(SIGN_CERT) $(SIGN_PWD)

install: $(APP).apk
	$(ADT) -installApp -platform android -platformsdk $(ANDROID_SDK) -package $(APP).apk

$(APP)Web.swf: $(SOURCES)
	haxe \
	-cp src \
	-cp vendor \
	-swf-version $(SWF_VERSION) \
	-swf-header 1920:1080:60:ffffff \
	-main Startup \
	-swf $(APP)Web.swf \
	-swf-lib vendor/gamepadWeb.swc \
	-resource assets/maps/test_map.tmx@test_map \
	-resource assets/maps/map_2.tmx@map_2 \
	-resource assets/maps/map_5.tmx@map_5 \
	-resource assets/maps/map_6.tmx@map_6 \
	-resource assets/maps/map_3.tmx@map_3 \
	-swf-lib vendor/starling.swc --macro "patchTypes('vendor/starling.patch')"

#$(APP).swf: $(SOURCES)
#	haxe \
#	-cp src \
#	-cp vendor \
#	-swf-version $(SWF_VERSION) \
#	-swf-header 1280:720:60:ffffff \
#	-main Startup \
#	-lib air3 \
#	-lib box2d \
#	-resource assets/maps/test_map.tmx@test_map \
#	-resource assets/maps/map_2.tmx@map_2 \
#	-resource assets/maps/map_5.tmx@map_5 \
#	-resource assets/maps/map_6.tmx@map_6 \
#	-resource assets/maps/map_3.tmx@map_3 \
#	-swf-lib vendor/starling.swc --macro "patchTypes('vendor/starling.patch')" \
#	-swf-lib vendor/gamepad.swc \
#	-swf $(APP).swf 

$(APP).apk: $(APP).swf sign.pfx
	$(ADT) -package -target apk-captive-runtime -storetype pkcs12 -keystore $(SIGN_CERT) $(APP).apk $(APP_XML) $(APP).swf src_assets assets src game_icon.png





