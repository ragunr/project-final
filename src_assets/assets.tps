<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>3</int>
        <key>texturePackerVersion</key>
        <string>3.6.0</string>
        <key>fileName</key>
        <string>E:/Projects/project-final/src_assets/assets.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>premultiplyAlpha</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>sparrow</string>
        <key>textureFileName</key>
        <filename>../assets/assets.png</filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>2</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>2</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>reduceBorderArtifacts</key>
        <false/>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">AnySize</enum>
            <key>forceSquared</key>
            <false/>
            <key>forceWordAligned</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>data</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../assets/assets.xml</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>cleanTransparentPixels</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>innerPadding</key>
            <uint>0</uint>
            <key>extrude</key>
            <uint>0</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Trim</enum>
            <key>heuristicMask</key>
            <false/>
            <key>pivotPoint</key>
            <enum type="SpriteSettings::PivotPoint">Center</enum>
        </struct>
        <key>fileList</key>
        <array>
            <filename>tilemap</filename>
            <filename>bellhopPlaceholder.png</filename>
            <filename>bellhopstanding.png</filename>
            <filename>bigger.png</filename>
            <filename>blacktie.png</filename>
            <filename>body1.png</filename>
            <filename>body2.png</filename>
            <filename>buisness woman standing copy.png</filename>
            <filename>businessmanPlaceholder.png</filename>
            <filename>japanese_0.png</filename>
            <filename>MonsterRoar01.png</filename>
            <filename>MonsterRoar02.png</filename>
            <filename>MonsterRoar03.png</filename>
            <filename>MonsterRoar04.png</filename>
            <filename>MonsterRoar05.png</filename>
            <filename>MonsterRoar06.png</filename>
            <filename>MonsterRoar07.png</filename>
            <filename>MonsterRoar08.png</filename>
            <filename>MonsterRoar09.png</filename>
            <filename>MonsterRoar10.png</filename>
            <filename>MonsterRoar11.png</filename>
            <filename>MonsterRoar12.png</filename>
            <filename>MonsterRoar13.png</filename>
            <filename>MonsterRoar14.png</filename>
            <filename>MonsterRoar15.png</filename>
            <filename>MonsterWalk01.png</filename>
            <filename>MonsterWalk02.png</filename>
            <filename>MonsterWalk03.png</filename>
            <filename>MonsterWalk04.png</filename>
            <filename>MonsterWalk05.png</filename>
            <filename>MonsterWalk06.png</filename>
            <filename>MonsterWalk07.png</filename>
            <filename>MonsterWalk08.png</filename>
            <filename>MonsterWalk09.png</filename>
            <filename>MonsterWalk10.png</filename>
            <filename>MonsterWalk11.png</filename>
            <filename>MonsterWalk12.png</filename>
            <filename>MonsterWalk13.png</filename>
            <filename>new.png</filename>
            <filename>npcPlaceholder.png</filename>
            <filename>pipe1Placeholder.png</filename>
            <filename>pipe2Placeholder.png</filename>
            <filename>pipeAndValvePlaceholder.png</filename>
            <filename>playerTestDark.png</filename>
            <filename>playerTestLit.png</filename>
            <filename>Pro_walk01.png</filename>
            <filename>Pro_walk02.png</filename>
            <filename>Pro_walk03.png</filename>
            <filename>Pro_walk04.png</filename>
            <filename>Pro_walk05.png</filename>
            <filename>Pro_walk06.png</filename>
            <filename>Pro_walk07.png</filename>
            <filename>Pro_walk08.png</filename>
            <filename>Pro_walk09.png</filename>
            <filename>Pro_walk10.png</filename>
            <filename>ProClimb01.png</filename>
            <filename>ProClimb02.png</filename>
            <filename>ProClimb03.png</filename>
            <filename>ProClimb04.png</filename>
            <filename>ProClimb05.png</filename>
            <filename>ProClimb06.png</filename>
            <filename>ProClimb07.png</filename>
            <filename>ProClimb08.png</filename>
            <filename>prostanding.png</filename>
            <filename>teenstanding.png</filename>
            <filename>titlefont.png</filename>
            <filename>valvePlaceholder.png</filename>
            <filename>menus/titlebuttons.png</filename>
            <filename>bellhoptalking01.png</filename>
            <filename>bellhoptalking02.png</filename>
            <filename>bellhoptalking03.png</filename>
            <filename>bellhoptalking04.png</filename>
            <filename>bellhoptalking05.png</filename>
            <filename>bellhoptalking06.png</filename>
            <filename>buisnesstalking01.png</filename>
            <filename>buisnesstalking02.png</filename>
            <filename>buisnesstalking03.png</filename>
            <filename>buisnesstalking04.png</filename>
            <filename>buisnesstalking05.png</filename>
            <filename>buisnesstalking06.png</filename>
            <filename>buisnesstalking07.png</filename>
            <filename>buisnesstalking08.png</filename>
            <filename>buisnesstalking09.png</filename>
            <filename>buisnesstalking10.png</filename>
            <filename>buisnesstalking11.png</filename>
            <filename>LedgeClimb01.png</filename>
            <filename>LedgeClimb02.png</filename>
            <filename>LedgeClimb03.png</filename>
            <filename>LedgeClimb04.png</filename>
            <filename>LedgeClimb05.png</filename>
            <filename>LedgeClimb06.png</filename>
            <filename>LedgeClimb07.png</filename>
            <filename>LedgeClimb08.png</filename>
            <filename>LedgeClimb09.png</filename>
            <filename>LedgeClimb10.png</filename>
            <filename>LedgeClimb11.png</filename>
            <filename>LedgeClimb12.png</filename>
            <filename>LedgeClimb13.png</filename>
            <filename>LedgeClimb14.png</filename>
            <filename>NpcWalka01.png</filename>
            <filename>NpcWalka02.png</filename>
            <filename>NpcWalka03.png</filename>
            <filename>NpcWalka04.png</filename>
            <filename>NpcWalka05.png</filename>
            <filename>NpcWalka06.png</filename>
            <filename>NpcWalka07.png</filename>
            <filename>NpcWalka08.png</filename>
            <filename>NpcWalka09.png</filename>
            <filename>NpcWalka10.png</filename>
            <filename>NpcWalkb01.png</filename>
            <filename>NpcWalkb02.png</filename>
            <filename>NpcWalkb03.png</filename>
            <filename>NpcWalkb04.png</filename>
            <filename>NpcWalkb05.png</filename>
            <filename>NpcWalkb06.png</filename>
            <filename>NpcWalkb07.png</filename>
            <filename>NpcWalkb08.png</filename>
            <filename>NpcWalkb09.png</filename>
            <filename>NpcWalkb10.png</filename>
            <filename>ProIdleB01.png</filename>
            <filename>ProIdleB02.png</filename>
            <filename>ProIdleB03.png</filename>
            <filename>ProIdleB04.png</filename>
            <filename>ProIdleB05.png</filename>
            <filename>ProIdleB06.png</filename>
            <filename>ProIdleB07.png</filename>
            <filename>ProIdleB08.png</filename>
            <filename>ProIdleB09.png</filename>
            <filename>ProIdleB10.png</filename>
            <filename>ProIdleTurn01.png</filename>
            <filename>ProIdleTurn02.png</filename>
            <filename>ProIdleTurn03.png</filename>
            <filename>ProTurn01.png</filename>
            <filename>ProTurn02.png</filename>
            <filename>ProTurn03.png</filename>
            <filename>ProTurn04.png</filename>
            <filename>ProTurn05.png</filename>
            <filename>ProTurn06.png</filename>
            <filename>Proturnwheel01.png</filename>
            <filename>Proturnwheel02.png</filename>
            <filename>Proturnwheel03.png</filename>
            <filename>Proturnwheel04.png</filename>
            <filename>Proturnwheel05.png</filename>
            <filename>Proturnwheel06.png</filename>
            <filename>body1_dark.png</filename>
            <filename>body2_dark.png</filename>
            <filename>chevrons.png</filename>
            <filename>door1.png</filename>
            <filename>door1_dark.png</filename>
            <filename>door2.png</filename>
            <filename>door2_dark.png</filename>
            <filename>GuyAtDesk1.png</filename>
            <filename>GuyAtDesk2.png</filename>
            <filename>NpcWalkaStand.png</filename>
            <filename>NpcWalkbStand.png</filename>
            <filename>ProHit01.png</filename>
            <filename>ProHit02.png</filename>
            <filename>ProHit03.png</filename>
            <filename>ProHit04.png</filename>
            <filename>ProHit05.png</filename>
            <filename>ProHit06.png</filename>
            <filename>GuyAtDesk1_dark.png</filename>
            <filename>GuyAtDesk2_dark.png</filename>
            <filename>TeenPul01.png</filename>
            <filename>TeenPul02.png</filename>
            <filename>TeenPul03.png</filename>
            <filename>TeenPul04.png</filename>
            <filename>teenagerwalk01.png</filename>
            <filename>teenagerwalk02.png</filename>
            <filename>teenagerwalk03.png</filename>
            <filename>teenagerwalk04.png</filename>
            <filename>teenagerwalk05.png</filename>
            <filename>teenagerwalk06.png</filename>
            <filename>teenagerwalk07.png</filename>
            <filename>teenagerwalk08.png</filename>
            <filename>teenagerwalk09.png</filename>
            <filename>teenagerwalk10.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
    </struct>
</data>
